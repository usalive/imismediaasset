import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

/* -------------------- Components ------------------------ */
import { AdAdjustmentComponent } from './pages/components/AdAdjustment/AdAdjustment.component';
import { IssueDatesComponent } from './pages/components/issueDates/issueDates.component';
import { MediaAssetsComponent } from './pages/components/mediaAssets/mediaAssets.component';
import { RateCardComponent } from './pages/components/rateCard/rateCard.component';
import { RateCardDetailsFinalComponent } from './pages/components/rateCardDetailsFinal/rateCardDetailsFinal.component';
import { InventoryDetailsComponent } from './pages/components/inventoryDetails/inventoryDetails.component';
import { SafePipe } from './pages/components/rateCardDetailsFinal/safe.pipe';
import { NumberDirective } from './pages/components/inventoryDetails/numbers-only.directive';

/* -------------------- services ------------------------ */
import { MediaAssetsService } from './pages/services/mediaAssets.service';
import { InventoryDetailsService } from './pages/services/inventory-details.service';
import { DataTablesModule } from '../../node_modules/angular-datatables';
import { GlobalClass } from './GlobalClass';

@NgModule({
    declarations: [
        AppComponent,
        AdAdjustmentComponent,
        IssueDatesComponent,
        RateCardDetailsFinalComponent,
        MediaAssetsComponent,
        RateCardComponent,
        InventoryDetailsComponent,
        SafePipe,
        NumberDirective
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),
        DataTablesModule,
        NgSelectModule
    ],
    providers: [
        GlobalClass,
        MediaAssetsService,
        InventoryDetailsService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
