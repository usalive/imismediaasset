import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RateCardModel } from '../../model/rateCardModel';
import { RateCardService } from '../../services/rateCard.service';
import { environment } from '../../../../environments/environment';
import { GlobalClass } from 'src/app/GlobalClass';
import { ToastrService } from 'ngx-toastr';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MediaAssetsService } from '../../services/mediaAssets.service';
declare var $, jQuery: any;
import { AppComponent } from '../../../app.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-rateCard',
  templateUrl: './rateCard.component.html',
  styleUrls: ['./rateCard.component.css']
})
export class RateCardComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  rateCardForm: FormGroup;
  rateCardModel = new RateCardModel();
  mediaAssetsId: any;
  mediaAssetsName: any;
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  message = [];

  adTypeModalForm: FormGroup;

  rateCarddtOptions: DataTables.Settings = {};
  rateCarddtTrigger: Subject<any> = new Subject();

  constructor(private rateCardService: RateCardService, private globalClass: GlobalClass,
    private formBuilder: FormBuilder, private toastr: ToastrService, private mediaAssetsService: MediaAssetsService,
    private appComponent: AppComponent) { }

  ngOnInit() {
    this.rateCarddtOptions = {
      pagingType: 'full_numbers',
      lengthChange: false, 
      searching: false,
      destroy: true
    };
    this.getMediaAssetsId();
    this.getRateCard('');
    this.getToken();
    this.adType();
    this.mediaType();
    this.mediaBilling();
    this.validation();
  }
  get isValidRateCardName() {
    return this.rateCardForm.get('RateCardName').invalid && (this.rateCardModel.israteCardFormSubmitted || this.rateCardForm.get('RateCardName').dirty || this.rateCardForm.get('RateCardName').touched);
  }
  get isValidAdType() {
    return this.rateCardForm.get('AdType').invalid && (this.rateCardModel.israteCardFormSubmitted || this.rateCardForm.get('AdType').dirty || this.rateCardForm.get('AdType').touched);
  }
  get isValidMediaType() {
    return this.rateCardForm.get('MediaType').invalid && (this.rateCardModel.israteCardFormSubmitted || this.rateCardForm.get('MediaType').dirty || this.rateCardForm.get('MediaType').touched);
  }

  get isValidMediaBillingMethod() {
    return this.rateCardForm.get('MediaBillingMethod').invalid && (this.rateCardModel.israteCardFormSubmitted || this.rateCardForm.get('MediaBillingMethod').dirty || this.rateCardForm.get('MediaBillingMethod').touched);
  }
  get isValidFromDate() {
    return this.rateCardForm.get('ValidFromDate').invalid && (this.rateCardModel.israteCardFormSubmitted || this.rateCardForm.get('ValidFromDate').dirty || this.rateCardForm.get('ValidFromDate').touched);
  }

  get isValidToDate() {
    return this.rateCardForm.get('ValidToDate').invalid && (this.rateCardModel.israteCardFormSubmitted || this.rateCardForm.get('ValidToDate').dirty || this.rateCardForm.get('ValidToDate').touched);
  }
  get isValidAdTypeName() {
    return this.adTypeModalForm.get('AdTypeName').invalid && (this.rateCardModel.isAdTypeModalFormSubmitted || this.adTypeModalForm.get('AdTypeName').dirty || this.adTypeModalForm.get('AdTypeName').touched);
  }
  
  ngOnDestroy() {
    this.rateCarddtTrigger.unsubscribe();
  }

  validation() {
    this.rateCardFormValidation();
    this.adTypeModalFormValidation();
  }
  
  rateCardFormValidation() {
    this.rateCardForm = this.formBuilder.group({
      RateCardName: [null, Validators.required],
      AdType: [null, Validators.required],
      MediaType: [null, Validators.required],
      MediaBillingMethod: [null, Validators.required],
      ValidFromDate: [null, Validators.required],
      ValidToDate: [null, Validators.required]
    });
  }

  adTypeModalFormValidation() {
    this.adTypeModalForm = this.formBuilder.group({
      AdTypeName: [null, Validators.required]
    });
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
    this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
     // this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getMediaAssetsId() {
    try {
      this.mediaAssetsId = this.globalClass.getMediaAssetID()[0];
      this.mediaAssetsName = this.globalClass.getMediaAssetID()[1];
      if (this.mediaAssetsId === undefined || this.mediaAssetsId == null || this.mediaAssetsId === '') {
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  searchRateCard()
  {
      let searchMedia='';
      searchMedia=(document.getElementById("txtSearchRateCard") as HTMLInputElement).value;
      this.getRateCard(searchMedia);
  }

  // Bind media asset data into list
  getRateCard(searchText) {
    try {
      this.showLoader();
      this.rateCardModel.RateCardData=[];
      this.rateCardService.getRateCardBySearchText(this.mediaAssetsId,searchText).subscribe(
        data => {
          this.hideLoader();
          this.rateCardModel.RateCardData = data.Data[0].RateCards;
          this.rateCarddtTrigger.next();
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // 1st Dropdown Bind
  adType() {
    try {
      this.rateCardService.getAdType().subscribe(
        data => {
          this.rateCardModel.AdTypeData = data.Data;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // 2nd Dropdown Bind
  mediaType() {
    try {
      this.rateCardService.getMediaType().subscribe(
        data => {
          this.rateCardModel.MediaTypeData = data.Data;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // 3rd Dropdown Bind
  mediaBilling() {
    try {
      this.rateCardService.getMediaBilling().subscribe(
        data => {
          this.rateCardModel.MediaBillingData = data.Data;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  resetRateCard() {
    this.rateCardModel.RateCardId = 0;
    this.rateCardForm.reset();
    document.querySelector('#btnRateCard').innerHTML = 'Create';
    document.getElementById('btnRateCard').style.display = 'inline-block';
  }

  /*---------------------------------------------------------*/
  editRateCard(RateCardId, RateCardName, AdTypeName, AdTypeId, MediaTypeName,
    MediaTypeId, MediaBillingMethodName, MediaBillingMethodId, ValidFromDate, ValidToDate) {
      try {
        
    this.rateCardModel.RateCardId = RateCardId;
    this.rateCardForm.controls['RateCardName'].setValue(RateCardName);
    this.rateCardForm.controls['AdType'].setValue(AdTypeId);
    this.rateCardForm.controls['MediaType'].setValue(MediaTypeId);
    this.rateCardForm.controls['MediaBillingMethod'].setValue(MediaBillingMethodId);
    this.rateCardForm.controls['ValidFromDate'].setValue(new Date(ValidFromDate));
    this.rateCardForm.controls['ValidToDate'].setValue(new Date(ValidToDate));
    document.querySelector('#btnRateCard').innerHTML = 'Update';
    document.getElementById('btnRateCard').style.display = 'inline-block';
      } catch (error) {
        console.log(error);
      }
      
  }

  copyRateCard(RateCardName, AdTypeId,
    MediaTypeId, MediaBillingMethodId, ValidFromDate, ValidToDate) {
      try {
        this.rateCardForm.controls['RateCardName'].setValue(RateCardName);
        this.rateCardForm.controls['AdType'].setValue(AdTypeId);
        this.rateCardForm.controls['MediaType'].setValue(MediaTypeId);
        this.rateCardForm.controls['MediaBillingMethod'].setValue(MediaBillingMethodId);
        this.rateCardForm.controls['ValidFromDate'].setValue(new Date(ValidFromDate));
        this.rateCardForm.controls['ValidToDate'].setValue(new Date(ValidToDate));
        document.querySelector('#btnRateCard').innerHTML = 'Create';
        document.getElementById('btnRateCard').style.display = 'inline-block';
      } catch (error) {
        console.log(error);
      }
  }

  saveRateCard() {
    try {
      const RateCardName = this.rateCardForm.controls['RateCardName'].value;
      const AdType = this.rateCardForm.controls['AdType'].value;
      const MediaType = this.rateCardForm.controls['MediaType'].value;
      const MediaBillingMethod = this.rateCardForm.controls['MediaBillingMethod'].value;
      let ValidFromDate = this.rateCardForm.controls['ValidFromDate'].value;
      let ValidToDate = this.rateCardForm.controls['ValidToDate'].value;
      this.rateCardModel.israteCardFormSubmitted = true;
      if (ValidToDate >= ValidFromDate) {
        ValidFromDate = this.globalClass.getDate(ValidFromDate);
        ValidToDate = this.globalClass.getDate(ValidToDate);
        if (this.rateCardForm.valid) {
          if (this.rateCardModel.RateCardId === 0) {
            this.rateCardService.addRateCard(RateCardName, AdType, MediaType, MediaBillingMethod, ValidFromDate,
              ValidToDate, this.mediaAssetsId).subscribe(result => {
                if (result.StatusCode === 1) {
                  try {
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.destroy();
                    });
                  } catch (error) {
                    this.hideLoader();
                    console.log(error);
                  }
                  this.getRateCard(''); 
                  this.rateCardModel.israteCardFormSubmitted = false;
                  this.toastr.success('Saved successfully.', 'Success!');
                  this.resetRateCard();
                } else {
                  this.rateCardModel.israteCardFormSubmitted = false;
                  this.toastr.error(result.Message, 'Error');
                }
              });
          } else {
            this.rateCardService.updateRateCard(this.rateCardModel.RateCardId,
              RateCardName, AdType, MediaType, MediaBillingMethod, ValidFromDate,
              ValidToDate, this.mediaAssetsId).subscribe(result => {
                if (result.StatusCode === 1) {
                  try {
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.destroy();
                    });
                  } catch (error) {
                    this.hideLoader();
                    console.log(error);
                  }
                  this.getRateCard('');
                  this.rateCardModel.israteCardFormSubmitted = false;
                  this.toastr.success('Saved successfully.', 'Success!');
                  this.resetRateCard();
                } else {
                  this.rateCardModel.israteCardFormSubmitted = false;
                  this.toastr.error(result.Message, 'Error');
                }
              });
          }
        }
      } else {
        this.toastr.error('To Date should be greater than From Date', 'Error');
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  delete(RateCardId, idx) {
    try {
      if (confirm('Are you sure you want to delete ')) {
        this.rateCardService.deleteMessage(RateCardId).subscribe(data => {
          if (data.StatusCode === 1) {
            try {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
              });
            } catch (error) {
              this.hideLoader();
              console.log(error);
            }
            this.getRateCard('');
            this.toastr.success('Deleted Successfully', 'Success!');
            this.resetRateCard();
          } else {
            this.toastr.error(data.Message, 'Error');
          }
        }, error => {
          this.hideLoader();
          if (error.status === 0) {
            this.toastr.error('Internal Server Error !');
          } else {
            this.toastr.error('Something went wrong.', 'Error');
          }
        });
      }
    } catch (error) {
      console.log(error);
    }

   
  }

  saveAdType() {
    try {
      this.rateCardModel.isAdTypeModalFormSubmitted = true;
      const adTypeName = this.adTypeModalForm.controls['AdTypeName'].value;
      this.rateCardService.addAdType(adTypeName).subscribe(
        data => {
          $('[data-dismiss=modal]').trigger({ type: 'click' });
          $('[data-dismiss=modal]').trigger({ type: 'click' });
          if (data.StatusCode === 1) {
            this.adType();
            this.toastr.success('Saved successfully.', 'Success!');
            this.rateCardModel.isAdTypeModalFormSubmitted = false;
            this.adTypeModalForm.reset();
          } else {
            this.toastr.error(data.Message, 'Error');
          }
        },
        error => {
          this.hideLoader();
          $('[data-dismiss=modal]').trigger({ type: 'click' });
          $('[data-dismiss=modal]').trigger({ type: 'click' });
          this.rateCardModel.isAdTypeModalFormSubmitted = false;
          if (error.status === 0) {
            this.toastr.error('Internal Server Error !');
          } else {
            this.toastr.error('Something went wrong.', 'Error');
          }
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  redirectToRateCardsDetails(RateCardId, RateCardName, AdTypeName) {
    this.globalClass.setRateCardDetails(RateCardId, RateCardName, AdTypeName, this.mediaAssetsId, this.mediaAssetsName);
    this.mediaAssetsService.set_ActiveTab('rateCardDetails');
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

}
