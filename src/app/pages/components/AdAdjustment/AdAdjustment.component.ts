import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { AdAdjustmentModel } from '../../model/adAdjustmentModel';
import { AdAdjustmentService } from '../../services/AdAdjustment.service';
import { MediaAssetsService } from '../../services/mediaAssets.service';
import { ToastrService } from 'ngx-toastr';
import { GlobalClass } from '../../../GlobalClass';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AppComponent } from '../../../app.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-AdAdjustment',
  templateUrl: './AdAdjustment.component.html',
  styleUrls: ['./AdAdjustment.component.css']
})

export class AdAdjustmentComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective)

  dtElement: DataTableDirective;
  adAdjustmentsForm: FormGroup;
  adAdjustmentModel = new AdAdjustmentModel();

  mediaAssetsId: any;
  mediaAssetsName: any;
  message = [];
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  addOREdit = 'Add';
  hasNext = true;
  offset = 0;

  adAdjustmentdtOptions: DataTables.Settings = {};
  adAdjustmentdtTrigger: Subject<any> = new Subject();

  constructor(private adAdjustmentService: AdAdjustmentService,
    private mediaAssetsService: MediaAssetsService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private globalClass: GlobalClass,
    private appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.adAdjustmentsFormValidation();
    this.adAdjustmentdtOptions = {
      pagingType: 'full_numbers',
      lengthChange: false,
      searching: false,
      destroy: true
    };

    this.getMediaAssetsId();
    this.getAdjustmentData('');
    this.getAdjustmentTarget();
    this.getWRI();
    this.getMediaAssetItemData();
  }

  get isValidProductCode(){
    return this.adAdjustmentsForm.get('ProductCode').invalid && (this.adAdjustmentModel.isadAdjustmentsFormSubmitted || this.adAdjustmentsForm.get('ProductCode').dirty || this.adAdjustmentsForm.get('ProductCode').touched);
  }
  get isValidAdjustment(){
    return this.adAdjustmentsForm.get('Adjustment').invalid && (this.adAdjustmentModel.isadAdjustmentsFormSubmitted || this.adAdjustmentsForm.get('Adjustment').dirty || this.adAdjustmentsForm.get('Adjustment').touched);
  }
  get isValidAdjustmentsTarget(){
    return this.adAdjustmentsForm.get('AdjustmentsTarget').invalid && (this.adAdjustmentModel.isadAdjustmentsFormSubmitted || this.adAdjustmentsForm.get('AdjustmentsTarget').dirty || this.adAdjustmentsForm.get('AdjustmentsTarget').touched);
  }
  get isValidAdjustmentsValue(){
    return this.adAdjustmentsForm.get('AdjustmentsValue').invalid && (this.adAdjustmentModel.isadAdjustmentsFormSubmitted || this.adAdjustmentsForm.get('AdjustmentsValue').dirty || this.adAdjustmentsForm.get('AdjustmentsValue').touched);
  }
  get isValidAmountPercentage(){
    return this.adAdjustmentsForm.get('AmountPercent').invalid && (this.adAdjustmentModel.isadAdjustmentsFormSubmitted || this.adAdjustmentsForm.get('AmountPercent').dirty || this.adAdjustmentsForm.get('AmountPercent').touched);
  }
  get isValidSurchargeDiscount(){
    return this.adAdjustmentsForm.get('SurchargeDiscount').invalid && (this.adAdjustmentModel.isadAdjustmentsFormSubmitted || this.adAdjustmentsForm.get('SurchargeDiscount').dirty || this.adAdjustmentsForm.get('SurchargeDiscount').touched);
  }
 

  ngOnDestroy() {
    this.adAdjustmentdtTrigger.unsubscribe();
  }

  adAdjustmentsFormValidation() {
    this.adAdjustmentsForm = this.formBuilder.group({
      ProductCode: [null, Validators.required],
      Adjustment: [null, Validators.required],
      AmountPercent: [null, Validators.required],
      SurchargeDiscount: [null, Validators.required],
      AdjustmentsTarget: [null, Validators.required],
      AdjustmentsValue: [null, Validators.required]
    });
  }
  getWRI() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
    this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
    //  this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getMediaAssetsId() {
    try {
      this.mediaAssetsId = this.globalClass.getMediaAssetID()[0];
      this.mediaAssetsName = this.globalClass.getMediaAssetID()[1];
      if (this.mediaAssetsId === undefined || this.mediaAssetsId == null || this.mediaAssetsId === '') {
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  searchAdAdjustment()
  {
      let searchText='';
      searchText=(document.getElementById("txtSearchAdAdjustement") as HTMLInputElement).value;
      this.getAdjustmentData(searchText);
  }

  // Get Adjustment Data
  getAdjustmentData(searchText) {
    try {
      this.showLoader();
      this.adAdjustmentModel.AdjustmentData=[];
      this.adAdjustmentService.getAdjustmentByMediaAssetNSearchText(this.mediaAssetsId,searchText).subscribe(data => {
        this.hideLoader();
        this.adAdjustmentModel.AdjustmentData = data.Data[0].AdAdjustments;
        this.adAdjustmentdtTrigger.next();
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Bind 1st drop down
  getMediaAssetItemData() {
    try {
      this.mediaAssetsService.getAllItemSummary(this.websiteRoot, this.reqVerificationToken, this.offset).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          for (const item of ItemData) {
            this.adAdjustmentModel.MediaAssetItemCode.push(item);
          }
          this.hasNext = result.HasNext;
          this.offset = result.Offset;
          const NextOffset = result.NextOffset;

          if (this.hasNext === true) {
            this.offset = NextOffset;
            this.getMediaAssetItemData();
          } else {
          }
        } else {
          this.adAdjustmentModel.MediaAssetItemCode = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }

  }

  // Bind 2nd drop down of
  getAdjustmentTarget() {
    try {
      this.adAdjustmentService.getAdjustmentTarget().subscribe(data => {
        this.adAdjustmentModel.adAdjustmentsTargets = data.Data;
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  adAddAdjustmentReset() {
    try {
      this.adAdjustmentModel.AdAdjustmentId = 0;
      this.adAdjustmentsForm.reset();
      document.querySelector('#btnAdAdjustment').innerHTML = 'Create';
      document.getElementById('btnAdAdjustment').style.display = 'inline-block';
    } catch (error) {
      console.log(error);
    }
  }

  editadAdjustments(AdAdjustmentId, ProductCode, AdAdjustmentName, AmountPercent,
    SurchargeDiscount, AdAdjustmentTargetName, AdjustmentValue) {
    try {
      this.adAdjustmentModel.AdAdjustmentId = AdAdjustmentId;

      if (AmountPercent === 0) {
        AmountPercent = 'Amount';
      } else if(AmountPercent===1) {
        AmountPercent = 'Percent';
      }

      if (SurchargeDiscount === 0) {
        SurchargeDiscount = 'Surcharge';
      } else if( SurchargeDiscount===1) {
        SurchargeDiscount = 'Discount';
      }

      this.adAdjustmentsForm.controls['ProductCode'].setValue(ProductCode);
      this.adAdjustmentsForm.controls['Adjustment'].setValue(AdAdjustmentName);
      this.adAdjustmentsForm.controls['AmountPercent'].setValue(AmountPercent);
      this.adAdjustmentsForm.controls['SurchargeDiscount'].setValue(SurchargeDiscount);
      this.adAdjustmentsForm.controls['AdjustmentsTarget'].setValue(AdAdjustmentTargetName);
      this.adAdjustmentsForm.controls['AdjustmentsValue'].setValue(AdjustmentValue);
      document.querySelector('#btnAdAdjustment').innerHTML = 'Update';
      document.getElementById('btnAdAdjustment').style.display = 'inline-block';
    } catch (error) {
      console.log(error);
    }
  }Adjustment

  saveAdjustments() {
    try {
      let validAdjustmentValue= true;
      this.adAdjustmentModel.isadAdjustmentsFormSubmitted = true;
      const productCode = this.adAdjustmentsForm.controls['ProductCode'].value;
      const adjustment = this.adAdjustmentsForm.controls['Adjustment'].value;
      let amountPercent = this.adAdjustmentsForm.controls['AmountPercent'].value;
      let surchargeDiscount = this.adAdjustmentsForm.controls['SurchargeDiscount'].value;
      const adjustmentstarget = this.adAdjustmentsForm.controls['AdjustmentsTarget'].value;
      const adjustmentsvalue = this.adAdjustmentsForm.controls['AdjustmentsValue'].value;

      if(isNaN(Number(adjustmentsvalue)))
      {
          validAdjustmentValue=false;
      }

      if(amountPercent !== null)
      {
        if (amountPercent === 'Amount') {
          amountPercent = 0;
        } else {
          amountPercent = 1;
        }
      }
      
      if(surchargeDiscount !== null)
      {
        if (surchargeDiscount === 'Surcharge') {
          surchargeDiscount = 0;
        } else {
          surchargeDiscount = 1;
        }
      }

      if(validAdjustmentValue)
      {
        if (this.adAdjustmentModel.AdAdjustmentId === 0) {
          this.adAdjustmentService.saveAdjustments(amountPercent, surchargeDiscount, this.mediaAssetsId, productCode, adjustment,
            adjustmentstarget, adjustmentsvalue).subscribe(result => {
              if (result.StatusCode === 1) {
                try {
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                  });
                } catch (error) {
                  this.hideLoader();
                  console.log(error);
                }
                this.getAdjustmentData('');
                this.adAdjustmentModel.isadAdjustmentsFormSubmitted = false;
                this.toastr.success('Saved successfully.', 'Success!');
                this.adAddAdjustmentReset();
              } else {
                this.adAdjustmentModel.isadAdjustmentsFormSubmitted = false;
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });
        } else {
          this.addOREdit = 'Edit';
          this.adAdjustmentService.updateAdjustments(this.adAdjustmentModel.AdAdjustmentId, amountPercent,
            surchargeDiscount, this.mediaAssetsId, productCode, adjustment,
            adjustmentstarget, adjustmentsvalue).subscribe(result => {
              if (result.StatusCode === 1) {
                try {
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                  });
                } catch (error) {
                  this.hideLoader();
                  console.log(error);
                }
  
                this.getAdjustmentData('');
  
                this.adAdjustmentModel.isadAdjustmentsFormSubmitted = false;
                this.toastr.success('Saved successfully.', 'Success!');
                this.adAddAdjustmentReset();
              } else {
                this.adAdjustmentModel.isadAdjustmentsFormSubmitted = false;
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });
        }
      }
      else
      {
        this.toastr.error('Please enter valid adjustments value in numeric format', 'Error');
      }

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  delete(AdAdjustmentId, idx) {
    try {
      if (confirm('Are you sure you want to delete ')) {
        this.adAdjustmentService.deleteMessage(AdAdjustmentId).subscribe(data => {

          if (data.StatusCode === 1) {
            try {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
              });
            } catch (error) {
              this.hideLoader();
              console.log(error);
            }

            this.getAdjustmentData('');

            this.toastr.success('Deleted Successfully', 'Success!');
            this.adAddAdjustmentReset();
          } else {
            this.toastr.error(data.Message, 'Error');
          }
        }, error => {
          this.hideLoader();
          if (error.status === 0) {
            this.toastr.error('Internal Server Error !');
          } else {
            this.toastr.error('Something went wrong.', 'Error');
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

}
