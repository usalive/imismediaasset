import { Component, OnInit, ɵConsole } from '@angular/core';
import { RateCardDetailsService } from '../../services/rateCardDetails.service';
import { environment } from '../../../../environments/environment';
import { GlobalClass } from 'src/app/GlobalClass';
import 'select2';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RateCardDetailsModel } from '../../model/rateCardDetailsModel';
import { element } from 'protractor';
import { AppComponent } from 'src/app/app.component';
declare const $, jQuery: any;


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-rateCardDetailsFinal',
  templateUrl: './rateCardDetailsFinal.component.html',
  styleUrls: ['./rateCardDetailsFinal.component.css']
})
export class RateCardDetailsFinalComponent implements OnInit {
  adColorModalForm: FormGroup;
  adSizeModalForm: FormGroup;
  frequencyModalForm: FormGroup;
  rateCardDetailsForm: FormGroup;
  rateCardDetailsModel = new RateCardDetailsModel();
  tempRateCardDetailCost: any = {};
  rateCardDetails: any = {};
  rateCardsDetails: any = {};
  rateCardshowMsgs = false;
  selectedAdColorValue: any = [];
  selectedAdSizeValue: any = [];
  selectedFrequencyValue: any = [];
  colorName = 'Select ad color';
  isShowDefaultTable = true;
  rateCardId = 0;
  dynamicRowData = {};
  rateCardName = '';
  adTypeName = '';
  mediaAssetName = '';
  globalCostValue: any = {};
  pSelectedAdSizeValue = [];
  pSelectedFrequencyValue = [];
  mediaAssetId = 0;
  addOrEdit = 'Add';
  mediaBillingMethodName='';

  isMultipleRateCardsDetails = true;

  adColorId = 0;
  adColorName: any;
  adColors: any = [];
  adColorModalMsgs = false;

  adSize: any = {};
  adSizeId = 0;
  adSizes = [];
  adSizeModalMsgs = false;
  obj: any = {};
  tempobj: any = {};

  frequency: any = {};
  frequencyId = 0;
  adFrequencyModalMsgs = false;
  frequencies: any = [];
  isRateCardDetailsAlreadyExits = false;

  objSizeArray = [];
  objFrequencyArray = [];

  groupSetup = {
    multiple: true,

    formatSearching: 'Searching the ad size...',
    formatNoMatches: 'No ad size found'
  };

  groupSetup1 = {
    multiple: true,
    formatSearching: 'Searching the frequency...',
    formatNoMatches: 'No frequency found'
  };

  adColorSelect2 = {
    multiple: false,
    formatSearching: 'Searching the ad color...',
    formatNoMatches: 'No ad color found'
  };

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  rateCardsDetailsTblMulHTML = '';
  rateCardsDetailsTblTHHTML = '';
  rateCardsDetailsTableTbodyHTML = '';
  matrixCreated = false;

  constructor(private rateCardDetailsService: RateCardDetailsService,
    private globalClass: GlobalClass, private toastr: ToastrService, private formBuilder: FormBuilder,
    private appComponent: AppComponent) { }

  ngOnInit() {
    this.getToken();
    this.getRateCardName();
    this.getAllAdColor();
    this.getAllAdSize();
    this.getAllFrequency();
    this.getRateCardDetailsByRateCardId();
    this.validation();
  }

  get isValidAdColor() {
    return this.rateCardDetailsForm.get('AdColor').invalid && (this.rateCardDetailsModel.israteCardDetailsFormSubmitted || this.rateCardDetailsForm.get('AdColor').dirty || this.rateCardDetailsForm.get('AdColor').touched);
  }
  get isValidAdSizeValue() {
    return this.rateCardDetailsForm.get('AdSizeValue').invalid && (this.rateCardDetailsModel.israteCardDetailsFormSubmitted || this.rateCardDetailsForm.get('AdSizeValue').dirty || this.rateCardDetailsForm.get('AdSizeValue').touched);
  }
  get isValidFrequencyValue() {
    return this.rateCardDetailsForm.get('FrequencyValue').invalid && (this.rateCardDetailsModel.israteCardDetailsFormSubmitted || this.rateCardDetailsForm.get('FrequencyValue').dirty || this.rateCardDetailsForm.get('FrequencyValue').touched);
  }
  get isValidAdColorName() {
    return this.adColorModalForm.get('AdColorName').invalid && (this.rateCardDetailsModel.isAdColorModalFormSubmitted || this.adColorModalForm.get('AdColorName').dirty || this.adColorModalForm.get('AdColorName').touched);
  }
  get isValidAdSizeName() {
    return this.adSizeModalForm.get('AdSizeName').invalid && (this.rateCardDetailsModel.isAdSizeModalFormSubmitted || this.adSizeModalForm.get('AdSizeName').dirty || this.adSizeModalForm.get('AdSizeName').touched);
  }
  get isValidPageFraction() {
    return this.adSizeModalForm.get('PageFraction').invalid && (this.rateCardDetailsModel.isAdSizeModalFormSubmitted || this.adSizeModalForm.get('PageFraction').dirty || this.adSizeModalForm.get('PageFraction').touched);
  }
  get isValidFrequencyName() {
    return this.frequencyModalForm.get('FrequencyName').invalid && (this.rateCardDetailsModel.isFrequencyModalFormSubmitted || this.frequencyModalForm.get('FrequencyName').dirty || this.frequencyModalForm.get('FrequencyName').touched);
  }
  validation() {
    this.adColorModalFormValidation();
    this.adSizeModalFormValidation();
    this.frequencyModalFormValidation();
    this.rateCardDetailsFormValidation();
  }
  adColorModalFormValidation() {
    this.adColorModalForm = this.formBuilder.group({
      AdColorName: [null, Validators.required]
    });
  }
  adSizeModalFormValidation() {
    this.adSizeModalForm = this.formBuilder.group({
      AdSizeName: [null, Validators.required],
      PageFraction: [null, Validators.required]
    });
  }
  frequencyModalFormValidation() {
    this.frequencyModalForm = this.formBuilder.group({
      FrequencyName: [null, Validators.required]
    });
  }
  rateCardDetailsFormValidation() {
    this.rateCardDetailsForm = this.formBuilder.group({
      AdColor: [null, Validators.required],
      AdSizeValue: [null, Validators.required],
      FrequencyValue: [null, Validators.required]
    });
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
    this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
     // this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Get the list of all ad colors from Database
  /// </summary>
  getRateCardName() {
    try {
      this.rateCardId = this.globalClass.getRateCardDetails()[0];
      this.rateCardName = this.globalClass.getRateCardDetails()[1];
      this.adTypeName = this.globalClass.getRateCardDetails()[2];
      this.mediaAssetId = this.globalClass.getRateCardDetails()[3];
      this.mediaAssetName = this.globalClass.getRateCardDetails()[4];
      if (this.mediaAssetId === undefined || this.mediaAssetId == null) {
      }
      this.getMediaBillingMethodByRateCardId();
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }
  /// <summary>
  /// Get the list of all ad colors from Database
  /// </summary>
  getAllAdColor() {
    try {
      this.rateCardDetailsService.getAdColor().subscribe(
        data => {
          this.adColors = data.Data;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Get the list of all ad sizes from Database
  /// </summary>
  getAllAdSize() {
    try {
      this.rateCardDetailsService.getAdSizes(this.mediaAssetId).subscribe(
        data => {
          this.adSizes = data.Data;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getMediaBillingMethodByRateCardId()
  {
    try {
      this.rateCardDetailsService.getRateCardMediaBillingMethod(this.rateCardId).subscribe(
        data => {
          this.mediaBillingMethodName=data.Data.MediaBillingMethodName;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }


  /// <summary>
  /// Get the list of all frequencies from Database
  /// </summary>
  getAllFrequency() {
    try {
      this.rateCardDetailsService.getFrequency().subscribe(
        data => {
          this.frequencies = data.Data;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Get the list of frequencies from Database
  /// </summary>
  getAllFrequencyWithFilter() {
    try {
      this.rateCardDetailsService.getFrequency().subscribe(
        data => {
          this.frequencies = data.Data;

          this.selectedFrequencyValue.forEach((fId, index) => {
            const Id = fId;
            this.frequency.forEach((fItem, key2) => {
              if (Id === fItem.FrequencyId) {
                this.frequency.pop(fItem);
              }
            });
          });
          this.frequencies = this.frequency;
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }


  /// <summary>
  /// Get the rate card detail from Database by Rate Card Detail Id
  /// </summary>
  getRateCardDetailsByRateCardId() {
    try {
      this.rateCardDetailsService.getByRateCardId(this.rateCardId).subscribe(
        result => {
          if (result.StatusCode === 1) {
            if (result.Data.length > 0) {
              this.isShowDefaultTable = true;
              this.rateCardsDetails.RateCardId = result.Data[0].RateCardId;
              this.rateCardsDetails.AdTypeId = result.Data[0].AdTypeId;
              this.mediaAssetId = result.Data[0].MediaAssetId;
              this.rateCardsDetails.MediaAssetId = this.mediaAssetId;

              this.rateCardName = result.Data[0].RateCardName;
              this.adTypeName = result.Data[0].AdTypeName;
              this.mediaAssetName = result.Data[0].MediaAssetName;

              if (result.Data[0].RateCardDetails.length > 0) {

                result.Data[0].RateCardDetails.forEach((value, key) => {
                  this.makeDynamicArraytoCreateRateCardsDetailsTable(value, key);
                });
              } else {
                this.isMultipleRateCardsDetails = false;
              }
             
            } else {
              this.isShowDefaultTable = true;
              this.rateCardsDetails = {};
            }
          } else if (result.StatusCode === 3) {
            this.isShowDefaultTable = true;
            this.rateCardsDetails = {};
          } else {
          }
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// create dynamic array to create rate card details matrix
  /// </summary>
  makeDynamicArraytoCreateRateCardsDetailsTable(objMRateCardsDetails, key) {
    try {
      this.rateCardDetails = objMRateCardsDetails;
      const colorId = this.rateCardDetails.AdColorId.toString();
      const colorName = this.rateCardDetails.AdColorName;
      const objRateCardDetailCost = this.rateCardDetails.RateCardDetailCost;
      if (objRateCardDetailCost.length > 0) {
        this.isRateCardDetailsAlreadyExits = true;
        const AdSizeIdArray: any = [];
        const FrequencyIdArray: any = [];
        const AdSizeArray: any = [];
        const FrequencyArray: any = [];
        const AdColorIdArray: any = [];
        const RowDataArray: any = [];
        objRateCardDetailCost.forEach((costValue, key2) => {
          this.adSizeId = 0;
          this.frequencyId = 0;
          this.adColorId = 0;
          this.adSizeId = costValue.AdSizeId;
          this.frequencyId = costValue.FrequencyId;
          this.adColorId = costValue.AdColorId;

          if (AdSizeIdArray.indexOf(this.adSizeId) === -1) {
            this.tempobj = {};
            this.tempobj.AdSizeId = costValue.AdSizeId;
            this.tempobj.AdSizeName = costValue.AdSizeName;
            this.tempobj.RateCardDetailId = costValue.RateCardDetailId;
            AdSizeArray.push(this.tempobj);
            AdSizeIdArray.push(this.tempobj.AdSizeId);

          }
          if (FrequencyIdArray.indexOf(this.frequencyId) === -1) {
            this.tempobj = {};
            this.tempobj.FrequencyId = costValue.FrequencyId;
            this.tempobj.FrequencyName = costValue.FrequencyName;
            this.tempobj.RateCardDetailId = costValue.RateCardDetailId;
            FrequencyArray.push(this.tempobj);
            FrequencyIdArray.push(this.tempobj.FrequencyId);

          }
          if (AdColorIdArray.indexOf(this.adColorId) === -1) {
            AdColorIdArray.push(this.adColorId);
          }
          const dynamicModelName = 'sizeFrequency_' + costValue.AdSizeId + '_' + costValue.FrequencyId + '_' + colorId + '';
          RowDataArray[dynamicModelName] = costValue.RateCardCost;
        });


        const FIdArray = [];

        FrequencyIdArray.forEach((fvalue, key2) => {
          const id = String(fvalue);
          FIdArray.push(id);
        });

        const SIdArray = [];

        AdSizeIdArray.forEach((svalue, key2) => {
          const id = String(svalue);
          SIdArray.push(id);
        });

        this.pSelectedAdSizeValue = SIdArray;
        this.pSelectedFrequencyValue = FIdArray;
        this.createRateCardDetailsTableWithDataForMultiple(key, colorId, colorName, AdSizeIdArray, FrequencyIdArray,
          AdSizeArray, FrequencyArray, objRateCardDetailCost, RowDataArray);
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }


  /// <summary>
  /// Create multiple rate card details table with data
  /// </summary>
  createRateCardDetailsTableWithDataForMultiple(key, colorId, colorName, AdSizeIdArray, FrequencyIdArray, AdSizeArray,
    FrequencyArray, objRateCardDetailCost, RowDataArray) {
    try {
      const getAllSelectedFrequency = FrequencyIdArray;
      const getAllSelectedSize = AdSizeIdArray;
      if (key === 0) {
        $('#rateCardsDetailsTableforMultiple').empty();
      }

      const tempHTML = '<div class="rateCardTitle">' + colorName +
                      '</div> <div class="table-responsive custom-table rateCard-table">' +
                        '<table class="table" id="rateCardsDetailsTable">' +
                            '<thead>' +
                              '<tr id="rateCardsDetailsTableTH' + colorId + '"></tr>' +
                            '</thead>' +
                        '<tbody id="rateCardsDetailsTableTbody' + colorId + '"></tbody> ' +
                        '<tr *ngIf="isLoading == true"> <td>&nbsp;</td> <td>&nbsp;</td> </tr>' +
                        '</table>' +
                        '<div *ngIf="isLoading == dataLoading">'  +
                          '<div>' +
                          '</div>' +
                        '</div>' +
                      '</div>';

      this.rateCardsDetailsTblMulHTML += tempHTML;
      setTimeout(() => {
        if (getAllSelectedFrequency.length > 0) {
          let thElement = '<th>SPACE</th>';

          getAllSelectedFrequency.forEach((objFrequency, index) => {
            thElement = thElement + '<th style="text-align: center;">' + FrequencyArray[index].FrequencyName + '</th>';
          });

          $('#rateCardsDetailsTableTH' + colorId).append(thElement);
        }
      }, 100);

      setTimeout(() => {
      if (getAllSelectedFrequency.length > 0) {
        if (getAllSelectedSize.length > 0) {
          getAllSelectedSize.forEach((objSize, key1) => {
            let tdElement = '<td style="vertical-align: middle">' + AdSizeArray[key1].AdSizeName + '</td>';
            const Sizeid = objSize;
            this.globalCostValue = RowDataArray;
            getAllSelectedFrequency.forEach((objFrequency, key2) => {
              const dynamicModelName = 'sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '';

              const text = this.globalCostValue[dynamicModelName];
              tdElement = tdElement + '<td style="text-align: center;">' + text + '</td>';
            });

            tdElement = '<tr>' + tdElement + ' </tr>';
            $('#rateCardsDetailsTableTbody' + colorId).append(tdElement);
            this.isShowDefaultTable = false;
          });
        }
      }}, 200);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getRateCardsDetailsByRateCardAndColor(rateCardId, AdColorId) {
    try {
      const selectedAdColorId = AdColorId;
      this.rateCardDetailsService.getByRateCardAndColor(rateCardId, AdColorId).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data.length > 0) {
            this.isShowDefaultTable = false;
            this.rateCardDetails = result.Data[0];
            const objRateCardDetailCost = result.Data[0].RateCardDetailCost;
            if (objRateCardDetailCost.length > 0) {
              const AdSizeIdArray = [];
              const AdSizeNameArray = [];
              const AdSizeArray = [];
              const FrequencyIdArray = [];
              const FrequencyNameArray = [];
              const FrequencyArray = [];
              const AdColorIdArray = [];
              const RowDataArray = [];
              objRateCardDetailCost.forEach((costValue, key2) => {
                let AdSizeId = 0;
                let AdSizeName = '';
                let FrequencyId = 0;
                let FrequencyName = '';
                let AdColorId = 0;
                AdSizeId = costValue.AdSizeId;
                AdSizeName = costValue.AdSizeName;
                FrequencyId = costValue.FrequencyId;
                FrequencyName = costValue.FrequencyName;
                AdColorId = costValue.AdColorId;
                if (AdSizeIdArray.indexOf(AdSizeId) === -1) {
                  this.tempobj = {};
                  this.tempobj.AdSizeId = costValue.AdSizeId;
                  this.tempobj.Name = costValue.AdSizeName;
                  this.tempobj.RateCardDetailId = costValue.RateCardDetailId;
                  AdSizeArray.push(this.tempobj);
                  AdSizeIdArray.push(this.tempobj.AdSizeId);
                  AdSizeNameArray.push(this.tempobj.Name);
                }

                if (FrequencyIdArray.indexOf(FrequencyId) === -1) {
                  this.tempobj = {};
                  this.tempobj.FrequencyId = costValue.FrequencyId;
                  this.tempobj.Name = costValue.FrequencyName;
                  this.tempobj.RateCardDetailId = costValue.RateCardDetailId;
                  FrequencyArray.push(this.tempobj);
                  FrequencyIdArray.push(this.tempobj.FrequencyId);
                  FrequencyNameArray.push(this.tempobj.Name);
                }
                if (AdColorIdArray.indexOf(AdColorId) === -1) {
                  AdColorIdArray.push(AdColorId);
                }
                const dynamicModelName = 'sizeFrequency_' + costValue.AdSizeId + '_' +
                  costValue.FrequencyId + '_' + selectedAdColorId + '';
                RowDataArray[dynamicModelName] = costValue.RateCardCost;
              });
              const cId = AdColorIdArray[0].toString();
              this.rateCardDetails.AdColorId = cId;
              const SIdArray = [];
              AdSizeIdArray.forEach((svalue, key2) => {
                const id = String(svalue);
                SIdArray.push(id);
              });

              this.objSizeArray = [];
              AdSizeArray.forEach(element => {
                this.obj = {};
                this.obj.AdSizeId = element.AdSizeId;
                this.obj.Name = element.Name;
                this.objSizeArray.push(this.obj);
              });
              const FIdArray = [];
              FrequencyIdArray.forEach((fvalue, key2) => {
                const id = String(fvalue);
                FIdArray.push(id);
              });
              this.objFrequencyArray = [];
              FrequencyArray.forEach(element => {
                this.obj = {};
                this.obj.FrequencyId = element.FrequencyId;
                this.obj.Name = element.Name;
                this.objFrequencyArray.push(this.obj);
              });
              this.selectedFrequencyValue = FIdArray;
              this.selectedAdSizeValue = SIdArray;
              this.obj = {};
              $('#adjtarget1').data().select2.updateSelection(this.objSizeArray);
              $('#adjtarget2').data().select2.updateSelection(this.objFrequencyArray);
              this.createRateCardDetailsTableWithData(
                AdSizeIdArray,
                FrequencyIdArray,
                AdSizeArray,
                FrequencyArray,
                objRateCardDetailCost,
                RowDataArray);
            } else {
              if ($('#rateCardsDetailsTableTH').children().length !== -1) {
                $('#rateCardsDetailsTableTH').empty();
              }
              if ($('#rateCardsDetailsTableTbody').children().length !== -1) {
                $('#rateCardsDetailsTableTbody').empty();
              }
              this.isShowDefaultTable = true;
            }
          } else {
            this.isShowDefaultTable = true;
            this.rateCardsDetails = {};
          }
        } else if (result.StatusCode === 3) {
          this.isShowDefaultTable = true;
          this.rateCardsDetails = {};
        } else {
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Create rate card details table with data
  /// </summary>
  createRateCardDetailsTableWithData(AdSizeIdArray, FrequencyIdArray, AdSizeArray, FrequencyArray,
    objRateCardDetailCost, RowDataArray) {
    try {
      const getAllSelectedFrequency = FrequencyIdArray;
      const getAllSelectedSize = AdSizeIdArray;
      if (getAllSelectedFrequency.length > 0) {
        $('#rateCardsDetailsTableTH').empty();
        let thElement = '<th>SPACE</th>';
        getAllSelectedFrequency.forEach((objFrequency, key) => {
          thElement = thElement + '<th style="text-align: center;">' + FrequencyArray[key].Name + '</th>';
        });
        this.rateCardsDetailsTblTHHTML += thElement;
      }
      if (getAllSelectedFrequency.length > 0) {
        const colorId = objRateCardDetailCost[0].AdColorId;
        $('#rateCardsDetailsTableTbody').empty();
        if (getAllSelectedSize.length > 0) {
          this.dynamicRowData = new Array();
          getAllSelectedSize.forEach((objSize, key1) => {
            let tdElement = '<td style="vertical-align: middle">' + AdSizeArray[key1].Name + '</td>';
            const Sizeid = objSize;
            this.globalCostValue = RowDataArray;
            getAllSelectedFrequency.forEach((objFrequency, key2) => {
              const dynamicModelName = 'sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '';
              const text = this.globalCostValue[dynamicModelName];
              const the_string = 'dynamicRowData.sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '';
              tdElement = tdElement + '<td style="text-align: center;">' +
                          '<input type="text" allow-decimal-numbers  name="' + dynamicModelName +
                '" id="' + dynamicModelName + '" value="' + text + '" placeholder="Rate" /></td>';
            });
            tdElement = '<tr>' + tdElement + ' </tr>';
            this.rateCardsDetailsTableTbodyHTML += tdElement;
            this.isShowDefaultTable = false;
            this.matrixCreated = true;
          });
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

// change dropdown
changeSelect2DropDownColor(controlId, controlValue) {
  let select2ChosenDiv: any;
  if (controlValue !== '' && controlValue !== undefined && controlValue != null) {
      select2ChosenDiv = $('#' + controlId).find('.select2-chosen');
      select2ChosenDiv.css('color', '#323232');
  } else {
      select2ChosenDiv = $('#' + controlId).find('.select2-chosen');
      select2ChosenDiv.css('color', '#999999');
  }
}

  changeAdColor(colorName) {
    try {
      this.rateCardsDetailsTableTbodyHTML = '';
      this.rateCardsDetailsTblTHHTML = '';
      this.isMultipleRateCardsDetails = false;
      this.addOrEdit = 'Add';
      const rateCardId = this.rateCardId;
      const selectedAdColorId = this.rateCardDetailsForm.controls['AdColor'].value;
      const tempDtArrObj = this.adColors.find(s => s.AdColorId === selectedAdColorId);
      this.colorName =colorName; //tempDtArrObj.AdColorName;
      if (selectedAdColorId !== undefined && selectedAdColorId !== '' && selectedAdColorId !== 0 &&
        selectedAdColorId != null) {
        this.changeSelect2DropDownColor('s2id_ddlAdColor', selectedAdColorId);
      } else {
        this.changeSelect2DropDownColor('s2id_ddlAdColor', '');
      }
      this.selectedAdSizeValue = {};
      this.selectedFrequencyValue = {};
      $('#adjtarget1').select2('val', this.selectedAdSizeValue);
      $('#adjtarget2').select2('val', this.selectedFrequencyValue);
      this.isRateCardDetailsAlreadyExits = false;
      this.matrixCreated = false;
      this.rateCardshowMsgs = false;
      if (rateCardId != null && rateCardId !== undefined && selectedAdColorId != null &&
        selectedAdColorId !== undefined && selectedAdColorId !== '') {
        this.rateCardDetailsService.getByRateCardAndColor(rateCardId, selectedAdColorId).subscribe(result => {
          console.log(result);
          if (result.StatusCode === 1) {
            if (result.Data.length > 0) {
              this.isShowDefaultTable = false;
              this.rateCardDetails = result.Data[0];
              this.rateCardDetails.AdColorId = selectedAdColorId.toString();
              const objRateCardDetailCost = result.Data[0].RateCardDetailCost;
              if (objRateCardDetailCost.length > 0) {
                this.isRateCardDetailsAlreadyExits = true;
                const AdSizeIdArray = [];
                const AdSizeNameArray = [];
                const AdSizeArray = [];
                const FrequencyIdArray = [];
                const FrequencyNameArray = [];
                const FrequencyArray = [];
                const AdColorIdArray = [];
                const RowDataArray = [];
                objRateCardDetailCost.forEach((costValue, key2) => {
                  let AdSizeId = 0;
                  let AdSizeName = '';
                  let FrequencyId = 0;
                  let FrequencyName = '';
                  let AdColorId = 0;
                  AdSizeId = costValue.AdSizeId;
                  AdSizeName = costValue.AdSizeName;
                  FrequencyId = costValue.FrequencyId;
                  FrequencyName = costValue.FrequencyName;
                  AdColorId = costValue.AdColorId;
                  if (AdSizeIdArray.indexOf(AdSizeId) === -1) {
                    this.tempobj = {};
                    this.tempobj.AdSizeId = costValue.AdSizeId;
                    this.tempobj.Name = costValue.AdSizeName;
                    this.tempobj.RateCardDetailId = costValue.RateCardDetailId;
                    AdSizeArray.push(this.tempobj);
                    AdSizeIdArray.push(this.tempobj.AdSizeId);
                    AdSizeNameArray.push(this.tempobj.Name);
                  }

                  if (FrequencyIdArray.indexOf(FrequencyId) === -1) {
                    this.tempobj = {};
                    this.tempobj.FrequencyId = costValue.FrequencyId;
                    this.tempobj.Name = costValue.FrequencyName;
                    this.tempobj.RateCardDetailId = costValue.RateCardDetailId;
                    FrequencyArray.push(this.tempobj);
                    FrequencyIdArray.push(this.tempobj.FrequencyId);
                    FrequencyNameArray.push(this.tempobj.Name);
                  }
                  if (AdColorIdArray.indexOf(AdColorId) === -1) {
                    AdColorIdArray.push(AdColorId);
                  }
                  const dynamicModelName = 'sizeFrequency_' + costValue.AdSizeId + '_' +
                    costValue.FrequencyId + '_' + selectedAdColorId + '';
                  RowDataArray[dynamicModelName] = costValue.RateCardCost;
                });
                const cId = AdColorIdArray[0].toString();
                this.rateCardDetails.AdColorId = cId;
                const SIdArray = [];
                AdSizeIdArray.forEach((svalue, key2) => {
                  const id = String(svalue);
                  SIdArray.push(id);
                });
                this.objSizeArray = [];
                AdSizeArray.forEach(element => {
                  this.obj = {};
                  this.obj.AdSizeId = element.AdSizeId;
                  this.obj.Name = element.Name;
                  this.objSizeArray.push(this.obj);
                });
                const FIdArray = [];
                FrequencyIdArray.forEach((fvalue, key2) => {
                  const id = String(fvalue);
                  FIdArray.push(id);
                });
                this.objFrequencyArray = [];
                FrequencyArray.forEach(element => {
                  this.obj = {};
                  this.obj.FrequencyId = element.FrequencyId;
                  this.obj.Name = element.Name;
                  this.objFrequencyArray.push(this.obj);
                });
                this.pSelectedAdSizeValue = SIdArray;
                this.pSelectedFrequencyValue = FIdArray;
                this.obj = {};
                this.addOrEdit = 'Edit';
                this.createRateCardDetailsTableWithData(AdSizeIdArray, FrequencyIdArray, AdSizeArray,
                  FrequencyArray, objRateCardDetailCost, RowDataArray);
              } else {
                this.rateCardDetailsForm.controls['AdSizeValue'].setValue(null);
                this.rateCardDetailsForm.controls['FrequencyValue'].setValue(null);
                if ($('#rateCardsDetailsTableTH').children().length !== -1) {
                  $('#rateCardsDetailsTableTH').empty();
                }
                if ($('#rateCardsDetailsTableTbody').children().length !== -1) {
                  $('#rateCardsDetailsTableTbody').empty();
                }
                this.isShowDefaultTable = true;
              }
            } else {
              this.isShowDefaultTable = true;
              this.rateCardsDetails = {};
            }
          } else if (result.StatusCode === 3) {
            this.isShowDefaultTable = true;
            this.rateCardsDetails = {};
          } else {
            //   alert(result.message);
          }
        });
      } else {
        $('#rateCardsDetailsTableTH').empty();
        $('#rateCardsDetailsTableTbody').empty();
        this.isShowDefaultTable = true;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // function getRateCardsDetails
  saveRateCardDetails(rateCardDetails) {
    console.log("rate card details: "+rateCardDetails);
    try {
      const selectedAdColorId = this.rateCardDetailsForm.controls['AdColor'].value;
      const selectedAdSizeValue = this.rateCardDetailsForm.controls['AdSizeValue'].value;
      const selectedFrequencyValue = this.rateCardDetailsForm.controls['FrequencyValue'].value;
      const colorId = selectedAdColorId;
      const ArrayRateCardDetailCost = new Array();
      if (this.rateCardDetailsForm.valid) {
        if (selectedAdSizeValue.length > 0) {
          const objrateCardDetails = rateCardDetails.RateCardDetailCost;
          selectedAdSizeValue.forEach((objSizeValue, key1) => {
            objSizeValue = objSizeValue.AdSizeId;
            selectedFrequencyValue.forEach((objFrequencyValue, key2) => {
              objFrequencyValue = objFrequencyValue.FrequencyId;
              let rateCardDetailsId = 0;
              for (let i = 0, len = objrateCardDetails.length - 1; i <= len; i++) {
                const tempRateCardDetails = objrateCardDetails[i];
                if (tempRateCardDetails.AdSizeId === objSizeValue && tempRateCardDetails.FrequencyId === objFrequencyValue) {
                  rateCardDetailsId = tempRateCardDetails.RateCardDetailId;
                  break;
                }
              }
              this.tempRateCardDetailCost = {};
              if (rateCardDetailsId === 0) {
                this.tempRateCardDetailCost.RateCardDetailId = 0;
              } else {
                this.tempRateCardDetailCost.RateCardDetailId = rateCardDetailsId;
              }
              this.tempRateCardDetailCost.AdSizeId = objSizeValue;
              this.tempRateCardDetailCost.FrequencyId = objFrequencyValue;

              const SaveRateCardCost = $('#sizeFrequency_' + objSizeValue + '_' + objFrequencyValue + '_' + colorId).val();
                if (SaveRateCardCost === '' || SaveRateCardCost === undefined || SaveRateCardCost == null) {
                  this.tempRateCardDetailCost.RateCardCost = 0;
                } else {
                  if(isNaN(Number(SaveRateCardCost)))
                  {
                      this.toastr.error('Please enter valid rate in numeric format', 'Error');
                  }
                  else{
                    this.tempRateCardDetailCost.RateCardCost = SaveRateCardCost;
                  }
                }
              ArrayRateCardDetailCost.push(this.tempRateCardDetailCost);
            });
          });
          rateCardDetails.RateCardDetailCost = ArrayRateCardDetailCost;
        }
      } else {
        this.rateCardshowMsgs = true;
      }


      if (this.rateCardDetailsForm.valid) {
        if (rateCardDetails.RateCardDetailCost.length > 0) {

          if (this.matrixCreated === true) {

            this.rateCardDetailsService.save(rateCardDetails).subscribe(result => {
            
              if (result.StatusCode === 1) {
              
                this.isRateCardDetailsAlreadyExits = true;
                this.rateCardDetails.AdColorId = selectedAdColorId.toString();
                this.getRateCardsDetailsByRateCardAndColor(this.rateCardId, selectedAdColorId);
                this.addOrEdit = 'Edit';
                this.toastr.success('Saved successfully.', 'Success!');
              } else {
                this.toastr.error(result.Message, 'Error');
              }
            });


          } else {
            this.toastr.info('Please first create matrix', 'Information');
          }
        }
      } else {
        this.rateCardshowMsgs = true;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  createRateCardDetailsTable() {
    try {
      this.rateCardsDetailsTblTHHTML = '';
      this.rateCardsDetailsTableTbodyHTML = '';
      let validCardDetail=true;
      const selectedAdColorId = this.rateCardDetailsForm.controls['AdColor'].value;
      const selectedAdSizeValue = this.rateCardDetailsForm.controls['AdSizeValue'].value;
      const selectedFrequencyValue = this.rateCardDetailsForm.controls['FrequencyValue'].value;
      
      if(selectedAdColorId !== null && (selectedAdSizeValue === null || selectedFrequencyValue===null))
      {
          validCardDetail=false;
          this.toastr.error('Please select ad size and frequency', 'Error');
      }

      if (selectedAdSizeValue === undefined || selectedFrequencyValue === undefined) {
        this.isRateCardDetailsAlreadyExits = false;
        
      } else if (selectedAdSizeValue.length === 0 || selectedFrequencyValue.length === 0) {
        this.isRateCardDetailsAlreadyExits = false;
      }
      const tempDtArrObj = this.adColors.find(s => s.AdColorId === selectedAdColorId);
      let success = false;
      let mediaBillingNotPerWord = false;
      let additionalWordFound=false;
      if (this.isRateCardDetailsAlreadyExits === true) {
        success = confirm('Matrix already exist for ' + tempDtArrObj.Name + '.  Are you sure you want to update matrix?');
        success = true;
      }
      if (this.isRateCardDetailsAlreadyExits === false) {
        success = true;
      }

      if(this.mediaBillingMethodName==="Per Word")
      {
          mediaBillingNotPerWord=true;
          if(selectedAdSizeValue.length>2)
          {
            mediaBillingNotPerWord=false;
            //this.toastr.error('For per word rate card details you can only select ad size as for main word and additional word', 'Error');
          }
          else{
            selectedAdSizeValue.forEach((objSize, key1) => {
              if(objSize.Name==="Additional Word")
              {
                  additionalWordFound=true;
              }
              else{
                let secondElement = objSize.Name.split(' ');
                let validSecondElement = true;
                if(secondElement.length==2)
                {
                  console.log(isNaN(Number(secondElement[0])));
                  if(isNaN(Number(secondElement[0])))
                  {
                    validSecondElement=false;
                  }
                  //|| secondElement[1].toLowerCase()==="word"
                  if(secondElement[1].toLowerCase()==="words" && validSecondElement===true)
                  {validSecondElement=true;}
                  else
                  {
                    validSecondElement=false;
                  }
                  // for(var i=0;i<secondElement.length;i++)
                  // {
                       
                  // }
                }
                else{validSecondElement=false;}
                mediaBillingNotPerWord = validSecondElement;
              }
            });
          }
          // if(mediaBillingNotPerWord===false || additionalWordFound===false)
          // {
            
          // }
          if(selectedAdSizeValue.length<2)
          {
            mediaBillingNotPerWord=false;
            //this.toastr.error('For per word rate card details you can need to select 2 ad size', 'Error');
          }
          if(selectedFrequencyValue.length>1)
          {
            mediaBillingNotPerWord=false;
            //this.toastr.error('For per word rate card details you can only select one frequency', 'Error');
          }
      }
      else
      {
        mediaBillingNotPerWord=true;
      }

      if(mediaBillingNotPerWord===false && this.mediaBillingMethodName==="Per Word")
      {
        this.toastr.error('For Per Word billing method, you can add only two Ad size: Additional Word and N Words', 'Error');
      }


      if (success && mediaBillingNotPerWord) {
        if (this.rateCardDetailsForm.valid) {
          if ($('#adjtarget2').select2('data').length > 0) {
            if ($('#rateCardsDetailsTableTH').children().length !== -1) {
              let thElement = '<th>SPACE</th>';
              selectedFrequencyValue.forEach((objFrequency, key) => {
                thElement = thElement + '<th style="text-align: center;">' + objFrequency.Name + '</th>';
              });
              this.rateCardsDetailsTblTHHTML += thElement;
            }
          }

          if ($('#adjtarget2').select2('data').length > 0) {
            if ($('#rateCardsDetailsTableTbody').children().length !== -1) {
              if (selectedAdSizeValue.length > 0) {
                this.dynamicRowData = new Array();

                selectedAdSizeValue.forEach((objSize, key1) => {
                  let tdElement = '<td style="vertical-align: middle">' + objSize.Name + '</td>';
                  const sizeId = objSize.AdSizeId;

                  selectedFrequencyValue.forEach((objFrequency, key2) => {
                    const FrequencyId = objFrequency.FrequencyId;

                    const dynamicModelName = 'sizeFrequency_' + sizeId + '_' + FrequencyId + '_' + selectedAdColorId;
                    let text = this.globalCostValue[dynamicModelName];
                    const the_string = 'dynamicRowData.sizeFrequency_' + sizeId + '_' + FrequencyId + '_' + selectedAdColorId;

                    if (text === undefined) {
                      text = '';
                      if (this.pSelectedAdSizeValue.indexOf(sizeId) === -1) {
                        this.pSelectedAdSizeValue.push(sizeId);
                      }

                      if (this.pSelectedFrequencyValue.indexOf(FrequencyId) === -1) {
                        this.pSelectedFrequencyValue.push(FrequencyId);
                      }
                    }

                    tdElement = tdElement + '<td style="text-align: center;"><input type="text" allow-decimal-numbers ' +
                      // tslint:disable-next-line:max-line-length
                      '[(ngModel)]="' + the_string + '"' +
                      ' name="' + dynamicModelName + '"' +
                      ' id="' + dynamicModelName + '"' +
                      ' value="' + text + '" placeholder="Rate"/></td>';
                  });
                  tdElement = '<tr>' + tdElement + ' </tr>';
                  this.rateCardsDetailsTableTbodyHTML += tdElement;
                  this.isShowDefaultTable = false;
                  this.matrixCreated = true;
                });
              }
            }
          }
          this.rateCardshowMsgs = false;
        } else {
          this.rateCardshowMsgs = true;
          this.toastr.error('Select ad color, ad size and frequency in order to create rate card details matrix', 'Error');
        }
      } else {
        this.isShowDefaultTable = false;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  saveAdColor() {
    try {
      this.rateCardDetailsModel.isAdColorModalFormSubmitted = true;
      const AdColorName = this.adColorModalForm.controls['AdColorName'].value;
      console.log(AdColorName);
      if(AdColorName !== undefined && AdColorName !== null && AdColorName !== '')
      {
        this.rateCardDetailsService.addSaveAdColor(AdColorName).subscribe(
          data => {
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            if (data.StatusCode === 1) {
              this.getAllAdColor();
              this.toastr.success('Saved successfully.', 'Success!');
              this.rateCardDetailsModel.isAdColorModalFormSubmitted = false;
              this.adColorModalForm.reset();
            } else {
              this.toastr.error(data.Message, 'Error');
              this.adColorModalForm.reset();
            }
          },
          error => {
            this.hideLoader();
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            this.rateCardDetailsModel.isAdColorModalFormSubmitted = false;
            this.adColorModalForm.reset();
            if (error.status === 0) {
              this.toastr.error('Internal server error !');
            } else {
              this.toastr.error('Something went wrong. Please try again.', '');
            }
          }
        ); 
      }
      else{
        this.toastr.error('Please enter ad color', 'Error');
      }
    } catch (error) {
      this.hideLoader();      
      console.log(error);
    }
  }

  saveAdSize() {
    try {
      let success='true';
      this.rateCardDetailsModel.isAdSizeModalFormSubmitted = true;
      const AdSizeName = this.adSizeModalForm.controls['AdSizeName'].value;
      const PageFraction = this.adSizeModalForm.controls['PageFraction'].value;
      
      console.log(AdSizeName +' ' + PageFraction);
      if(AdSizeName==='' || AdSizeName === null || AdSizeName === undefined)
      {
        success='false';
        this.toastr.error('Please enter ad size.', 'Error');
      }
      if((PageFraction === '' || PageFraction === null || PageFraction === undefined) && success==='true')
      {
          success='false';
          this.toastr.error('Please enter page fraction', 'Error');
      }

      if(isNaN(Number(PageFraction)))
      {
        success='false';
          this.toastr.error('Please enter page fraction valid numeric values', 'Error');
      }

      if(success==='true')
      {
        this.rateCardDetailsService.addSaveAdSize(AdSizeName, PageFraction, this.mediaAssetId).subscribe(
          data => {
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            if (data.StatusCode === 1) {
              this.getAllAdSize();
              this.toastr.success('Saved successfully.', 'Success!');
              this.rateCardDetailsModel.isAdSizeModalFormSubmitted = false;
              this.adSizeModalForm.reset();
            } else {
              this.toastr.error(data.Message, 'Error');
            }
          },
          error => {
            this.hideLoader();
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            this.rateCardDetailsModel.isAdSizeModalFormSubmitted = false;
            this.adSizeModalForm.reset();
            if (error.status === 0) {
              this.toastr.error('Internal server error !');
            } else {
              this.toastr.error('Something went wrong. Please try again.', '');
            }
          }
        );
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  saveFrequency() {
    try {
      this.rateCardDetailsModel.isFrequencyModalFormSubmitted = true;
      const FrequencyName = this.frequencyModalForm.controls['FrequencyName'].value;
      if(FrequencyName !== '' && FrequencyName !== null && FrequencyName !== undefined)
      {
        this.rateCardDetailsService.addSaveFrequency(FrequencyName).subscribe(
          data => {
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            if (data.StatusCode === 1) {
              this.getAllFrequency();
              this.toastr.success('Saved successfully.', 'Success!');
              this.rateCardDetailsModel.isFrequencyModalFormSubmitted = false;
              this.frequencyModalForm.reset();
            } else {
              this.toastr.error(data.Message, 'Error');
            }
          },
          error => {
            this.hideLoader();
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            $('[data-dismiss=modal]').trigger({ type: 'click' });
            this.rateCardDetailsModel.isFrequencyModalFormSubmitted = false;
            this.frequencyModalForm.reset();
            if (error.status === 0) {
              this.toastr.error('Internal server error !');
            } else {
              this.toastr.error('Something went wrong. Please try again.', '');
            }
          }
        );
      }
      else{
        this.toastr.error('Please enter frequency name', 'Error');
      }
      
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeAdSizes() {
    try {
      this.matrixCreated = false;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeFrequencies() {
    try {
      this.matrixCreated = false;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}