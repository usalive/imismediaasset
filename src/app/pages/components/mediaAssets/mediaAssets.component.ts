import { AfterViewInit, Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MediaAssetsModel } from '../../model/MediaAssetsModel';
import { MediaAssetsService } from '../../services/mediaAssets.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../environments/environment';
import { GlobalClass } from '../../../GlobalClass';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AppComponent } from '../../../app.component';
declare var $, jQuery: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-mediaAssets',
  templateUrl: './mediaAssets.component.html',
  styleUrls: ['./mediaAssets.component.css']
})
export class MediaAssetsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  mediaAssetsdtTrigger: Subject<any> = new Subject();

  mediaAssetsOptions: DataTables.Settings = {};
  mediaAssetsForm: FormGroup;
  mediaAssetsModel = new MediaAssetsModel();

  dummyLogo: any;
  logo: any;
  image: File = null;
  message = [];

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';

  imgurl = '';
 
  hasNext = true;
  offset = 0;

  mediaAssetGroupForm: FormGroup;
  mediaAssetTypeForm :FormGroup;

  constructor(
    private globalClass: GlobalClass,
    private formBuilder: FormBuilder,
    private mediaAssetsService: MediaAssetsService,
    private toastr: ToastrService,
    private appComponent: AppComponent,
  ) { }


  

  ngOnInit() {
    this.mediaAssetsOptions = {
      pagingType: 'full_numbers',
      lengthChange: false, 
      searching: false,
      destroy: true
    };

    this.getToken();
    this.getMediaAsset('');
    this.getMediaAssetItemData();
    this.getMediaAssetGroupData();
    this.getMediaTypeData();

    this.validation();
  }
  get isValidMediaAsset() {
    return this.mediaAssetsForm.get('MediaAsset').invalid && (this.mediaAssetsModel.isMediaAssetsFormSubmitted || this.mediaAssetsForm.get('MediaAsset').dirty || this.mediaAssetsForm.get('MediaAsset').touched);
  }

  get isValidMediaCode() {
    return this.mediaAssetsForm.get('MediaCode').invalid && (this.mediaAssetsModel.isMediaAssetsFormSubmitted || this.mediaAssetsForm.get('MediaCode').dirty || this.mediaAssetsForm.get('MediaCode').touched);
  }
  get isValidProductCode() {
    return this.mediaAssetsForm.get('ProductCode').invalid && (this.mediaAssetsModel.isMediaAssetsFormSubmitted || this.mediaAssetsForm.get('ProductCode').dirty || this.mediaAssetsForm.get('ProductCode').touched);
  }
  get isValidMediaType() {
    return this.mediaAssetsForm.get('MediaType').invalid && (this.mediaAssetsModel.isMediaAssetsFormSubmitted || this.mediaAssetsForm.get('MediaType').dirty || this.mediaAssetsForm.get('MediaType').touched);
  }
  get isValidMediaGroup() {
    return this.mediaAssetsForm.get('MediaGroup').invalid && (this.mediaAssetsModel.isMediaAssetsFormSubmitted || this.mediaAssetsForm.get('MediaGroup').dirty || this.mediaAssetsForm.get('MediaGroup').touched);
  }
  get isValidMediaGroupName() {
    return this.mediaAssetGroupForm.get('MediaAssetGroupName').invalid && (this.mediaAssetsModel.ismediaAssetGroupFormSubmitted || this.mediaAssetGroupForm.get('MediaAssetGroupName').dirty || this.mediaAssetGroupForm.get('MediaAssetGroupName').touched);
  }

  get isValidMediaTypeName() {
    return this.mediaAssetTypeForm.get('MediaAssetTypeName').invalid && (this.mediaAssetsModel.ismediaAssetTypeFormSubmitted || this.mediaAssetTypeForm.get('MediaAssetTypeName').dirty || this.mediaAssetTypeForm.get('MediaAssetTypeName').touched);
  }
  ngAfterViewInit(): void {
    this.mediaAssetsdtTrigger.next();
  }

  ngOnDestroy() {
    this.mediaAssetsdtTrigger.unsubscribe();
  }

  validation() {
    this.mediaAssetsFormValidation();
    this.mediaAssetGroupFormValidation();
    this.mediaAssetTypeFormValidation();
  }
  mediaAssetsFormValidation() {
   
    this.mediaAssetsForm = this.formBuilder.group({
      MediaAsset: [null, Validators.required],
      MediaCode: [null, Validators.required],
      ProductCode: [null, Validators.required],
      MediaGroup: [null, Validators.required],
      MediaType: [null, Validators.required],
      image: [null]
    });
  }

  mediaAssetGroupFormValidation() {
    this.mediaAssetGroupForm = this.formBuilder.group({
      MediaAssetGroupName: [null, Validators.required]
    });
  }
  mediaAssetTypeFormValidation() {
    this.mediaAssetTypeForm = this.formBuilder.group({
      MediaAssetTypeName: [null, Validators.required]
    });
  }


  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      
          var pathArray = this.websiteRoot.split( '/' );
          var protocol = pathArray[0];
          var host = pathArray[2];
        this.websiteRoot = protocol + '//' + host + '/';
          if (this.websiteRoot.startsWith("http://"))
          this.websiteRoot = this.websiteRoot.replace('http://', 'https://');
      // this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  searchMedia()
  {
      let searchMedia='';
      searchMedia=(document.getElementById("txtSearchMedia") as HTMLInputElement).value;
      this.getMediaAsset(searchMedia);
  }

  getMediaAsset(searchText) {
    try {
      this.mediaAssetsModel.MediaAssetData=[];
      this.mediaAssetsService.getMediaAssetsViaSearch(searchText).subscribe(
        data => {
          this.hideLoader();
          this.mediaAssetsModel.MediaAssetData = data.Data;
          //console.log("mediaassetdata:"+JSON.stringify(this.mediaAssetsModel.MediaAssetData));
          this.mediaAssetsdtTrigger.next();
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Bind 1st drop down of modal popup
  getMediaAssetItemData() {
    try {
      this.mediaAssetsService.getAllItemSummary(
        this.websiteRoot,
        this.reqVerificationToken,
        this.offset
      ).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
         
            const ItemData = result.Items.$values;
            for (const item of ItemData) {
              this.mediaAssetsModel.MediaAssetItemCode.push(item);
            }
            this.hasNext = result.HasNext;
            this.offset = result.Offset;
            const NextOffset = result.NextOffset;
  
            if (this.hasNext === true) {
              this.offset = NextOffset;
              this.getMediaAssetItemData();
            } else {
            }
          } else {
            this.mediaAssetsModel.MediaAssetItemCode = [];
          }
        }, error => {
          this.hideLoader();
        });
    } catch (error) {
    console.log(error)  ;
    }
  }
 

  // Bind 2nd drop down of modal popup
  getMediaAssetGroupData() {
    try {
      this.mediaAssetsService.getMediaAssetGroupData().subscribe(data => {
        if (data.StatusCode === 1) {
          this.mediaAssetsModel.MediaAssetGroupData = data.Data;
        } else {
        }
      }, error => {
        this.hideLoader();
      });
    } catch (error) {
      console.log(error);
    }
  }

  // Bind 3rd drop down of modal popup
  getMediaTypeData() {
    try {
          
    this.mediaAssetsService.getMediaTypeData().subscribe(data => {
      if (data.StatusCode === 1) {
        this.mediaAssetsModel.MediaAssetTypeData = data.Data;
      } else {
      }
    }, error => {
      this.hideLoader();
    });
    } catch (error) {
      console.log(error);
    }

  }

  detectFiles(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {
        const reader = new FileReader();
        // tslint:disable-next-line:no-shadowed-variable
        reader.onload = (event: any) => {
          this.dummyLogo = event.target.result;
          this.logo = event.target.result.replace(/^data:image\/[a-z]+;base64,/, '');
        };
        reader.readAsDataURL(event.target.files[0]);
  
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);
        if (ext === 'jpg' || ext === 'jpeg' || ext === 'png') {
          this.image = event.target.files.item(0);
        } else {
          $('#image').val('');
          this.toastr.info('Please select only jpg, jpeg,and png file for logo ', 'Information!');
        }
      }
    } catch (error) {
      console.log(error);
    }

  
  }

  resetMediaAssetPopup() {
    try {
    this.dummyLogo = null;
    this.mediaAssetsModel.MediaAssetId = 0;
    this.mediaAssetsForm.reset();
    document.querySelector('#btnMediaAssets').innerHTML = 'Create Media Asset';
    document.getElementById('btnMediaAssets').style.display = 'inline-block';
    } catch (error) {
     console.log(error) ;
    }
    
  }

  getUpdateMediaAssetData(productCode, mediaAssetId, mediaAssetGroupId,
    mediaTypeId, mediaTypeName, logo, mediaGroupName, mediaAssetName,
    mediaCode) {
      try {
      
    this.mediaAssetsModel.MediaAssetId = mediaAssetId;
    this.mediaAssetsForm.controls['MediaAsset'].setValue(mediaAssetName);
    this.mediaAssetsForm.controls['MediaCode'].setValue(mediaCode);
    this.mediaAssetsForm.controls['ProductCode'].setValue(productCode);
    this.mediaAssetsForm.controls['MediaGroup'].setValue(mediaAssetGroupId);
    this.mediaAssetsForm.controls['MediaType'].setValue(mediaTypeId);
    this.dummyLogo = logo;
    document.querySelector('#btnMediaAssets').innerHTML = 'Update Media Asset';
    document.getElementById('btnMediaAssets').style.display = 'inline-block';
      } catch (error) {
        console.log(error);
      }
      
  }

  saveMediaAsset() {
    try {
     
      this.mediaAssetsModel.isMediaAssetsFormSubmitted = true;
      if (this.mediaAssetsForm.valid) {
        this.mediaAssetsModel.MediaAssetName = this.mediaAssetsForm.controls['MediaAsset'].value;
        this.mediaAssetsModel.MediaCode = this.mediaAssetsForm.controls['MediaCode'].value;
        this.mediaAssetsModel.ProductCode = this.mediaAssetsForm.controls['ProductCode'].value;
        this.mediaAssetsModel.MediaGroup = this.mediaAssetsForm.controls['MediaGroup'].value;
        this.mediaAssetsModel.MediaTypeName = this.mediaAssetsForm.controls['MediaType'].value;
        if (this.mediaAssetsModel.MediaAssetId === 0) {

          // Add new record
          this.mediaAssetsService.addMediaAssets(this.mediaAssetsModel, this.logo).subscribe(result => {
            if (result.StatusCode === 1) {
              $('[data-dismiss=modal]').trigger({ type: 'click' });
              $('[data-dismiss=modal]').trigger({ type: 'click' });
              try {
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  dtInstance.destroy();
                });
              } catch (error) {
                this.hideLoader();
                console.log(error);
              }

              this.mediaAssetsModel.isMediaAssetsFormSubmitted = false;
              this.logo = null;
              this.image = null;
              this.dummyLogo = null;
              this.getMediaAsset('');

              this.toastr.success('Saved successfully.', 'Success!');
              this.resetMediaAssetPopup();
            } else {
              this.mediaAssetsModel.isMediaAssetsFormSubmitted = false;
              this.logo = null;
              this.image = null;
              this.toastr.error(result.Message, 'Error!');
            }
          });
        } else {
          // Update record
          this.mediaAssetsService.updateMediaAssets(this.mediaAssetsModel, this.mediaAssetsModel.MediaAssetId,
            this.logo).subscribe(result => {
              if (result.StatusCode === 1) {
                $('[data-dismiss=modal]').trigger({ type: 'click' });
                $('[data-dismiss=modal]').trigger({ type: 'click' });
                try {
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                  });
                } catch (error) {
                  this.hideLoader();
                  console.log(error);
                }

                this.mediaAssetsModel.isMediaAssetsFormSubmitted = false;
                this.logo = null;
                this.image = null;
                this.dummyLogo = null;
                this.getMediaAsset('');
                this.toastr.success('Saved successfully.', 'Success!');
                this.resetMediaAssetPopup();
              } else {
                this.mediaAssetsModel.isMediaAssetsFormSubmitted = false;
                this.logo = null;
                this.image = null;
                this.toastr.error(result.Message, 'Error!');
              }
            });
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }
  saveMediaAssetTypeForm(){
    try {
      this.mediaAssetsModel.ismediaAssetTypeFormSubmitted = true;
      const MediaAssetTypeName = this.mediaAssetTypeForm.controls[
        'MediaAssetTypeName'
      ].value;

      this.mediaAssetsService.addType(MediaAssetTypeName).subscribe(
        data => {
          $('#mediaAssetTypeModal').modal('hide');

          if (data.StatusCode === 1) {
            this.getMediaTypeData();
            this.toastr.success('Saved successfully.', 'Success!');
            this.mediaAssetsModel.ismediaAssetTypeFormSubmitted = false;
            this.mediaAssetTypeForm.reset();
          } else {
            this.toastr.error(data.Message, 'Error');
          }
        },
        error => {
          this.hideLoader();
          $('#mediaAssetTypeModal').modal('hide');
          this.mediaAssetsModel.ismediaAssetTypeFormSubmitted = false;
          if (error.status === 0) {
            this.toastr.error('Internal server error !');
          } else {
            this.toastr.error('Something went wrong. please try again.', '');
          }
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }

  }

  delete(mediaAssetId, idx) {
    try {
          
    if (confirm('Are you sure you want to delete ?')) {
      this.mediaAssetsService.deleteMessage(mediaAssetId).subscribe(data => {
        if (data.StatusCode === 1) {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
            });
          } catch (error) {
            this.hideLoader();
            console.log(error);
          }

          this.getMediaAsset('');
          this.toastr.success('Deleted successfully.', 'Success!');
          this.resetMediaAssetPopup();
        } else {
          this.toastr.error(data.Message, 'Error!');
        }
      }, error => {
        this.hideLoader();
        if (error.status === 0) {
          this.toastr.error('Internal server error !');
        } else {
          this.toastr.error('Something went wrong.', 'Error');
        }
      });
    }
    } catch (error) {
      console.log(error);
    }

  }
  

  saveMediaAssetGroupForm() {
    try {
      this.mediaAssetsModel.ismediaAssetGroupFormSubmitted = true;
      const MediaAssetGroupName = this.mediaAssetGroupForm.controls[
        'MediaAssetGroupName'
      ].value;

      this.mediaAssetsService.addGroup(MediaAssetGroupName).subscribe(
        data => {
          $('#mediaAssetGroupModal').modal('hide');

          if (data.StatusCode === 1) {
            this.getMediaAssetGroupData();
            this.toastr.success('Saved successfully.', 'Success!');
            this.mediaAssetsModel.ismediaAssetGroupFormSubmitted = false;
            this.mediaAssetGroupForm.reset();
          } else {
            this.toastr.error(data.Message, 'Error');
          }
        },
        error => {
          this.hideLoader();
          $('#mediaAssetGroupModal').modal('hide');
          this.mediaAssetsModel.ismediaAssetGroupFormSubmitted = false;
          if (error.status === 0) {
            this.toastr.error('Internal server error !');
          } else {
            this.toastr.error('Something went wrong. Please try again.', '');
          }
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  issueDates(mediaAsstesID, mediaAssetName) {
    try {
      this.globalClass.setMediaAssetID(mediaAsstesID, mediaAssetName);
      this.mediaAssetsService.set_ActiveTab('IssueDate');
    } catch (error) {
      this.hideLoader();
    }
  }

  adjustment(mediaAsstesID, mediaAssetName) {
    try {
      this.globalClass.setMediaAssetID(mediaAsstesID, mediaAssetName);
      this.mediaAssetsService.set_ActiveTab('Adjustment');
    } catch (error) {
      this.hideLoader();
    }
  }

  ratecard(mediaAsstesID, mediaAssetName) {
    try {
      this.globalClass.setMediaAssetID(mediaAsstesID, mediaAssetName);
      this.mediaAssetsService.set_ActiveTab('rateCard');
    } catch (error) {
      this.hideLoader();
    }
  }

  inventory(mediaAsstesID, mediaAssetName) {
    try {
      this.globalClass.setMediaAssetID(mediaAsstesID, mediaAssetName);
      this.mediaAssetsService.set_ActiveTab('inventoryDetails');
    } catch (error) {
      this.hideLoader();
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}
