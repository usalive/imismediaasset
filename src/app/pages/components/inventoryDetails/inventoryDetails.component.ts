import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { GlobalClass } from '../../../GlobalClass';
import { RateCardDetailsService } from '../../services/rateCardDetails.service';
import { InventoryDetailsService } from '../../services/inventory-details.service';
import { AppComponent } from '../../../app.component';
import 'select2';
import * as alasql from 'alasql';
declare const $, jQuery: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-inventoryDetails',
  templateUrl: './inventoryDetails.component.html',
  styleUrls: ['./inventoryDetails.component.css']
})
export class InventoryDetailsComponent implements OnInit {

  inventoryDetailsForm: FormGroup;
  isInventoryDetailsFormSubmitted = false;

  inventoryDetailsTblTHHTML = '';
  inventoryDetailsTableTbodyHTML = '';
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  addOrEdit = 'Add';
  errorMessage = 'Something went wrong !';

  dynamicRowData: any = {};
  globalCostValue = [];
  tempInventoryDetail: any = {};

  pSelectedAdSizeValue = [];
  pSelectedIssueDateValue = [];
  issueDates = [];
  adSizes = [];
  objInventoryArr = [];

  isMultipleRateCardsDetails = true;
  isInventoryDetailsAlreadyExits = false;
  matrixCreated = true;
  isInventoryDetails = true;
  isAddOrEditButton = false;
  inventoryhowMsgs = false;
  applyAll = false;
  success = false;
  isShowDefaultTable = true;
  isLoading = true;
  dataLoading = true;
  isApplyCheckBoxShow = false;

  mediaAssetsId: any;
  mediaAssetName = '';
  updatedIssueDate=[];
  updatedAdSize=[];
  objIssueDates: any = {};
  objAdSize: any = {};
  tempCoverDate = [];

  fIdArray = [];
  sIdArray = [];

  inventoryDetails: any;

  constructor(private globalClass: GlobalClass,
    private formBuilder: FormBuilder,
    private rateCardDetailsService: RateCardDetailsService,
    private inventoryDetailsService: InventoryDetailsService,
    private toastr: ToastrService,
    private appComponent: AppComponent) { }

  ngOnInit() {

    this.inventoryDetailsFormValidation();
    this.getMediaAssetsId();
    this.getToken();

    this.getAllAdSize();
    this.getAllIssueDate();

    this.getInventoryDetailsByMediaAssetId();
  }
  get isValidIssueDate() {
    return this.inventoryDetailsForm.get('SelectedIssueDateValue').invalid && (this.isInventoryDetailsFormSubmitted || this.inventoryDetailsForm.get('SelectedIssueDateValue').dirty || this.inventoryDetailsForm.get('SelectedIssueDateValue').touched);
  }

  get isValidAdSize() {
    return this.inventoryDetailsForm.get('SelectedAdSizeValue').invalid && (this.isInventoryDetailsFormSubmitted || this.inventoryDetailsForm.get('SelectedAdSizeValue').dirty || this.inventoryDetailsForm.get('SelectedAdSizeValue').touched);
  }
  inventoryDetailsFormValidation() {
    this.inventoryDetailsForm = this.formBuilder.group({
      SelectedIssueDateValue: [null, Validators.required],
      SelectedAdSizeValue: [null, Validators.required]
    });
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
 
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split('/');
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
        this.websiteRoot = this.websiteRoot.replace('http', 'https');

      //   this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getMediaAssetsId() {
    try {
      this.mediaAssetsId = this.globalClass.getMediaAssetID()[0];
      this.mediaAssetName = this.globalClass.getMediaAssetID()[1];
      if (this.mediaAssetsId === undefined || this.mediaAssetsId == null || this.mediaAssetsId === '') {
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all ad sizes from Database
  getAllAdSize() {
    try {
      this.rateCardDetailsService.getAdSizes(this.mediaAssetsId).subscribe(
        result => {
          if (result.StatusCode === 1) {
            if (result.Data.length > 0) {
              this.adSizes = result.Data;
            }
          } else {
          }
        },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all frequencies from Database
  getAllIssueDate() {
    try {
      this.rateCardDetailsService.getByMediaAsset(this.mediaAssetsId).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data.length > 0) {
            if (result.Data[0].IssueDates.length > 0) {
              this.issueDates = alasql('SELECT * FROM ? AS add order by CoverDate desc', [result.Data[0].IssueDates]);
              for (let i = 0; i < this.issueDates.length; i++) {
                const tempCoverDate = this.issueDates[i]['CoverDate'];
                const convertCoverDate = this.globalClass.getDate(tempCoverDate);
                this.tempCoverDate.push(convertCoverDate);
                this.issueDates[i]['CoverDate'] = convertCoverDate;
              }
              this.isShowDefaultTable = false;
              this.dataLoading = false;
            } else {
              this.issueDates = [];
              this.isShowDefaultTable = true;
              this.dataLoading = false;
            }
          } else {
            this.issueDates = [];
            this.isShowDefaultTable = true;
            this.dataLoading = false;
          }
        } else if (result.StatusCode === 3) {
          this.issueDates = [];
          this.isShowDefaultTable = true;
          this.dataLoading = false;
        } else {
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the rate card detail from Database by Rate Card Detail Id
  getInventoryDetailsByMediaAssetId() {
    try {
      this.inventoryDetailsService.getByMediaAssetId(this.mediaAssetsId).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data.length > 0) {
            this.isShowDefaultTable = true;
            this.dataLoading = false;

            if (result.Data[0].Inventory.length > 0) {
              const mInventoryDetails = result.Data[0];
              this.inventoryDetails = result.Data[0];
              console.log("Inventory 1", this.inventoryDetails.Inventory)
              this.makeDynamicArraytoCreateInventoryDetailsTable(mInventoryDetails);

            } else {
              this.isMultipleRateCardsDetails = false;
            }
          } else {
            this.isShowDefaultTable = true;
            this.dataLoading = false;
          }
        } else if (result.StatusCode === 3) {
          this.isShowDefaultTable = true;
          this.dataLoading = false;
        } else {
          this.dataLoading = false;
        }

      },
        error => {
          this.hideLoader();
          console.log(error);
        }
      );
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // create dynamic array to create rate card details matrix
  makeDynamicArraytoCreateInventoryDetailsTable(objmInventoryDetails) {

    try {
      const inventoryDetails = objmInventoryDetails;
      const objInventory = inventoryDetails.Inventory;
      if (objInventory.length > 0) {
        this.isInventoryDetailsAlreadyExits = true;
        this.addOrEdit = 'Edit';
        // tslint:disable-next-line:max-line-length
        const AdSizeIdArray = [];
        const IssueDateIdArray = [];
        const AdSizeArray = [];
        const IssueDateArray = [];
        const RowDataArray = [];
        console.log("Inventory 2", objInventory)
        objInventory.forEach((costValue, key2) => {

          let AdSizeId = 0;
          let IssueDateId = 0;
          AdSizeId = costValue.AdSizeId;
          IssueDateId = costValue.IssueDateId;

          if (AdSizeIdArray.indexOf(AdSizeId) === -1) {
            const tempobj: any = {};
            tempobj.AdSizeId = costValue.AdSizeId;
            tempobj.AdSizeName = costValue.AdSizeName;
            tempobj.InventoryId = costValue.InventoryId;
            AdSizeArray.push(tempobj);
            AdSizeIdArray.push(tempobj.AdSizeId);
          }

          if (IssueDateIdArray.indexOf(IssueDateId) === -1) {
            const tempobj: any = {};
            tempobj.IssueDateId = costValue.IssueDateId;
            tempobj.IssueDate = this.globalClass.getDate(costValue.IssueDate);
            tempobj.InventoryId = costValue.InventoryId;
            IssueDateArray.push(tempobj);
            IssueDateIdArray.push(tempobj.IssueDateId);

          }
          const dynamicModelName = 'sizeIssueDate_' + costValue.AdSizeId + '_' + costValue.IssueDateId + '';
          RowDataArray[dynamicModelName] = costValue.TotalInventory;
        });

        const FNameArray = [];
        this.fIdArray = [];

        IssueDateArray.forEach((fvalue, key2) => {

          this.objIssueDates = {};
          console.log("IssueDateArray", IssueDateArray)
          if (this.issueDates.find(s => s.IssueDateId === fvalue.IssueDateId) !== undefined) {
            this.objIssueDates.IssueDateId = fvalue.IssueDateId;
            this.objIssueDates.CoverDate = fvalue.IssueDate;
            this.fIdArray.push(this.objIssueDates);
          }
        });
        const SNameArray = [];
        this.sIdArray = [];

        AdSizeArray.forEach((svalue, key2) => {

          this.objAdSize = {};
          if (this.adSizes.find(s => s.AdSizeId === svalue.AdSizeId) !== undefined) {
            this.objAdSize.AdSizeId = svalue.AdSizeId;
            this.objAdSize.Name = svalue.AdSizeName;
            this.sIdArray.push(this.objAdSize);
          }
        });

        this.pSelectedAdSizeValue = this.sIdArray;
        this.pSelectedIssueDateValue = this.fIdArray;

        this.createRateCardDetailsTableWithData(AdSizeIdArray, IssueDateIdArray, AdSizeArray, IssueDateArray,
          objInventory, RowDataArray);
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Create rate card details table with data
  createRateCardDetailsTableWithData(AdSizeIdArray, IssueDateIdArray, AdSizeArray, IssueDateArray, objInventory, RowDataArray) {
    try {

      const getAllSelectedIssueDate = IssueDateIdArray;
      const getAllSelectedSize = AdSizeIdArray;
      this.objInventoryArr = objInventory;

      if (getAllSelectedIssueDate.length > 0) {
        this.inventoryDetailsTblTHHTML = '';

        const tableHeader = new Array();
        let thElement = '<th></th>';

        getAllSelectedIssueDate.forEach((objIssueDate, key) => {
          thElement = thElement + '<th style="text-align: center;">' + IssueDateArray[key].IssueDate + '</th>';
        });

        this.inventoryDetailsTblTHHTML += thElement;
      }

      if (getAllSelectedIssueDate.length > 0) {
        this.inventoryDetailsTableTbodyHTML = '';

        if (getAllSelectedSize.length > 0) {
          this.dynamicRowData = new Array();

          getAllSelectedSize.forEach((objSize, key1) => {

            let tdElement = '<td style="vertical-align: middle">' + AdSizeArray[key1].AdSizeName + '</td>';
            const Sizeid = objSize;
            this.globalCostValue = RowDataArray;

            getAllSelectedIssueDate.forEach((objIssueDate, key2) => {
              const dynamicModelName = 'sizeIssueDate_' + Sizeid + '_' + objIssueDate + '';
              let text = this.globalCostValue[dynamicModelName];
              if (text == null || text === undefined) {
                text = '';
              } else {
                text = this.globalCostValue[dynamicModelName];
              }
              const the_string = 'dynamicRowData.sizeIssueDate_' + Sizeid + '_' + objIssueDate + '';
              tdElement = tdElement + '<td style="text-align: center;">' + text + '</td>';
            });

            tdElement = '<tr>' + tdElement + ' </tr>';
            this.inventoryDetailsTableTbodyHTML += tdElement;

            this.isShowDefaultTable = false;
            this.matrixCreated = true;
            this.dataLoading = false;
            this.isInventoryDetails = false;
            this.isAddOrEditButton = false;
          });
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  saveInventoryDetails() {
    try {
     
      // const selectedIssueDateValue = this.inventoryDetailsForm.controls['SelectedIssueDateValue'].value;
      // const selectedAdSizeValue = this.inventoryDetailsForm.controls['SelectedAdSizeValue'].value;
      const selectedIssueDateValue = this.updatedIssueDate;

      const selectedAdSizeValue =  this.updatedAdSize;

      
      console.log('Adsize',selectedAdSizeValue);
     console.log('IssueDate',selectedIssueDateValue);
      const inventoryDetails: any = {};
      if (inventoryDetails.Inventory === undefined) {
        inventoryDetails.Inventory = {};
      }
      const ArrayInventoryDetail = new Array();
      const issueDateID = new Array();
      selectedIssueDateValue.forEach((objIssueDateValue, key2) => {
        if (objIssueDateValue.IssueDateId != undefined || objIssueDateValue.IssueDateId != null) {
          issueDateID.push(objIssueDateValue.IssueDateId);
        }
      });
      if (this.inventoryDetailsForm.valid) {
        if (selectedAdSizeValue.length > 0) {
          const objinventoryDetails = inventoryDetails.Inventory;

          selectedAdSizeValue.forEach((objSizeValue, key1) => {

            try {
              if (objSizeValue.AdSizeId) {
                objSizeValue = objSizeValue.AdSizeId;
              }
            } catch (error) {
              this.hideLoader();
              objSizeValue = objSizeValue;
            }
            let inventoryDetailsId = 0;
            issueDateID.forEach((objIssueDateValue, key2) => {

              if (objIssueDateValue.IssueDateId != undefined || objIssueDateValue.IssueDateId != null) {
                objIssueDateValue = objIssueDateValue.IssueDateId;

                for (let i = 0, len = objinventoryDetails.length - 1; i <= len; i++) {
                  const tempInventoryDetails = objinventoryDetails[i];
                  if (tempInventoryDetails.AdSizeId === objSizeValue && tempInventoryDetails.IssueDateId === objIssueDateValue) {
                    inventoryDetailsId = tempInventoryDetails.InventoryId;
                    break;
                  }

                }
              }
              this.tempInventoryDetail = {};
              if (inventoryDetailsId === 0) {
                this.tempInventoryDetail.InventoryId = 0;
              } else {
                this.tempInventoryDetail.InventoryId = inventoryDetailsId;
              }

              this.tempInventoryDetail.AdSizeId = objSizeValue;

              this.tempInventoryDetail.IssueDateId = objIssueDateValue;
              const dynamicModelNameTemp = $('#sizeIssueDate_' + objSizeValue + '_' + objIssueDateValue).val();

              if (dynamicModelNameTemp === '' || dynamicModelNameTemp === undefined || dynamicModelNameTemp == null) {
                this.tempInventoryDetail.TotalInventory = 0;
              } else {
                this.tempInventoryDetail.TotalInventory = parseInt($.trim(dynamicModelNameTemp));
              }
              ArrayInventoryDetail.push(this.tempInventoryDetail);


            });
          });

          inventoryDetails.Inventory = ArrayInventoryDetail;

        }
      } else {
        this.inventoryhowMsgs = true;
      }

      if (this.inventoryDetailsForm.valid) {

        if (inventoryDetails.Inventory.length > 0) {
          if (this.matrixCreated === true) {
            inventoryDetails.mediaAssetsId = this.mediaAssetsId;
            this.showLoader();

            this.inventoryDetailsService.save(inventoryDetails).subscribe(result => {
              if (result.StatusCode === 1) {
                this.hideLoader();
                this.isInventoryDetailsAlreadyExits = true;
                this.applyAll = false;
                $('#chkApplyAll').prop('checked', false);
                this.addOrEdit = 'Edit';
                if (result.Message !== '' && result.Message != null) {
                  setTimeout(() => {
                    this.toastr.info('Saved successfully.' + '<br/><br/>' + result.Message);
                  }, 0);
                } else {
                  this.toastr.success('Saved successfully.', 'Success!');
                }
              } else {
                this.hideLoader();
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
              this.toastr.error(this.errorMessage, 'Error');
            });

          } else {
            this.toastr.info('Please first create matrix', 'Information');
          }
        }

      } else {
        this.inventoryhowMsgs = true;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  createInventoryDetailsTable() {
    try {
      if (this.inventoryDetailsForm.valid) {
        debugger;
        this.updatedIssueDate=this.inventoryDetailsForm.controls['SelectedIssueDateValue'].value;
        this.updatedAdSize=this.inventoryDetailsForm.controls['SelectedAdSizeValue'].value;
        const GetSelectedAdSize = this.inventoryDetailsForm.controls['SelectedAdSizeValue'].value;
        const GetSelectedIssueDate = this.inventoryDetailsForm.controls['SelectedIssueDateValue'].value;

        // Step 1
        if ((GetSelectedAdSize === undefined || GetSelectedAdSize === null || GetSelectedAdSize === '')
          || (GetSelectedIssueDate === undefined || GetSelectedIssueDate === null || GetSelectedIssueDate === '')) {
          this.isInventoryDetailsAlreadyExits = false;
        } else if (GetSelectedAdSize.length === 0 || GetSelectedIssueDate.length === 0) {
          this.isInventoryDetailsAlreadyExits = false;
        }

        if (this.isInventoryDetailsAlreadyExits === true) {
          this.success = confirm('Matrix already exist for ' + this.mediaAssetName + '.  Are you sure you want to update matrix?');
        }

        if (this.isInventoryDetailsAlreadyExits === false) {
          this.success = true;
        }

        if (this.success) {
          // Step 2
          if (GetSelectedIssueDate.length > 0) {
            if ($('#inventoryDetailsTableTH').children().length !== -1) {
              this.inventoryDetailsTblTHHTML = '';

              let thElement = '<th></th>';
              try {
                GetSelectedIssueDate.forEach((objIssueDate, key) => {
                  thElement = thElement + '<th style="text-align: center;">'
                    + this.globalClass.getTwoValDate(objIssueDate.CoverDate) + '</th>';
                });
              } catch (error) {
                this.hideLoader();
              }
              this.inventoryDetailsTblTHHTML += thElement;
            }
          }

          // Step 3
          const tempCheckbox = '';
          this.isApplyCheckBoxShow = false;
          if (GetSelectedAdSize.length > 0) {
            if ($('#inventoryDetailsTableTbody').children().length !== -1) {
              this.inventoryDetailsTableTbodyHTML = '';

              if (GetSelectedAdSize.length > 0) {

                this.dynamicRowData = new Array();

                GetSelectedAdSize.forEach((element, key1) => {
                  const tempAdArrObj = this.adSizes.find(s => s.AdSizeId === element.AdSizeId);
                  let tdElement = '<td style="vertical-align: middle">' + element.Name + '</td>';

                  GetSelectedIssueDate.forEach((objIssueDate, key2) => {

                    const dynamicModelName = 'sizeIssueDate_' + element.AdSizeId + '_' + objIssueDate.IssueDateId;
                    let txtTotalInventory = this.globalCostValue[dynamicModelName];

                    if (txtTotalInventory === undefined) {
                      txtTotalInventory = '';
                      if (this.pSelectedAdSizeValue.indexOf(element.AdSizeId) === -1) {
                        this.pSelectedAdSizeValue.push(element.AdSizeId);
                      }
                      if (this.pSelectedIssueDateValue.indexOf(objIssueDate.IssueDateId) === -1) {
                        this.pSelectedIssueDateValue.push(objIssueDate.IssueDateId);
                      }
                    }

                    tdElement = tdElement + '<td style="text-align: center;"><input type="text" ' +
                      '(ngModel)="dynamicRowData.sizeIssueDate_' + element.AdSizeId + '_' + objIssueDate.IssueDateId +
                      '" name="sizeIssueDate_' + element.AdSizeId + '_' + objIssueDate.IssueDateId + '"' +
                      'id="sizeIssueDate_' + element.AdSizeId + '_' + objIssueDate.IssueDateId +
                      '" value="' + $.trim(txtTotalInventory) + '" placeholder="Inv."  (click)="dynamicRowData.sizeIssueDate_' + element.AdSizeId +
                      '_' + objIssueDate.IssueDateId + ' = null" numbersOnly /></td>';

                    if (key2 === 0) {
                      if (GetSelectedIssueDate.length > 1) {
                        this.isApplyCheckBoxShow = true;
                      }
                    }

                  });

                  tdElement = '<tr>' + tdElement + ' </tr>';
                  if (key1 === GetSelectedAdSize.length - 1) {
                    tdElement = tdElement + ' <tr>' + tempCheckbox + ' </tr>';
                  }

                  this.inventoryDetailsTableTbodyHTML += tdElement;
                  this.isShowDefaultTable = false;
                  this.matrixCreated = true;
                  this.isInventoryDetails = false;
                  this.isAddOrEditButton = true;
                });

              }
            }
          }

          this.inventoryhowMsgs = false;

        } else {
          this.isShowDefaultTable = false;
        }
      } else {
        this.inventoryhowMsgs = true;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // change event
  changeIssueDate() {
    try {
      $('#ddlissuedate').select2('data');
      const IssueDateIds = $('#ddlissuedate').select2('data');
      const lastIssueDateLength = IssueDateIds.length - 1;
      const id = IssueDateIds[lastIssueDateLength];
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeAdSize() {
    try {
      const getAllSelectedSize = $('#ddladsize').select2('data');
      const sizeIds = $('#ddladsize').select2('val');
      const lastSizeLength = sizeIds.length - 1;
      const id = sizeIds[lastSizeLength];
    } catch (error) {
      this.hideLoader();
    }
  }

  checkboxChange(event: any) {
    try {
      if (event.target.checked) {
        const selectedIssueDateValue = this.inventoryDetailsForm.controls['SelectedIssueDateValue'].value;
        const selectedAdSizeValue = this.inventoryDetailsForm.controls['SelectedAdSizeValue'].value;
        if (selectedAdSizeValue.length > 0) {
          selectedAdSizeValue.forEach((objSizeValue, key1) => {
            try {
              if (objSizeValue.AdSizeId) {
                objSizeValue = objSizeValue.AdSizeId;
              }
            } catch (error) {
              this.hideLoader();
            }

            let dynamicTxtFirstVal = '';
            selectedIssueDateValue.forEach((objIssueDateValue, index) => {
              objIssueDateValue = objIssueDateValue.IssueDateId;
              if (index === 0) {
                dynamicTxtFirstVal = $('#sizeIssueDate_' + objSizeValue + '_' + objIssueDateValue).val();
              } else {
                $('#sizeIssueDate_' + objSizeValue + '_' + objIssueDateValue).val(dynamicTxtFirstVal);
              }
            });
          });
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

}
