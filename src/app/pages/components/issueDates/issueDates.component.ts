import { AfterViewInit, Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IssueDatesModel } from '../../model/issueDatesModel';
import { IssueDatesService } from '../../services/issueDates.service';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { GlobalClass } from '../../../GlobalClass';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AppComponent } from '../../../app.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-issueDates',
  templateUrl: './issueDates.component.html',
  styleUrls: ['./issueDates.component.css']
})

export class IssueDatesComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  issueDatesOptions: DataTables.Settings = {};
  issueDatesdtTrigger: Subject<any> = new Subject();

  issueDateForm: FormGroup;
  issueDatesModel = new IssueDatesModel();

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  message = [];
  addOREdit = 'ADD';

  mediaAssetsId: any;
  mediaAssetsName: any;

  maxAdClosingDate: Date;
  maxMaterialsDueDate: Date;

  constructor(private issueDatesService: IssueDatesService, private toastr: ToastrService, private formBuilder: FormBuilder,
    private globalClass: GlobalClass, private appComponent: AppComponent) {
  }

  ngOnInit() {
    this.getToken();
    this.issueDatesOptions = {
      pagingType: 'full_numbers',
      lengthChange: false,
      searching: false,
      destroy: true
    };
    this.getMediaAssetsId();
    this.getIssueDate('');
    this.issueDateFormValidation();
  }
  get isValidCoverDate(){
    return this.issueDateForm.get('CoverDate').invalid && (this.issueDatesModel.isIssueDateFormSubmitted || this.issueDateForm.get('CoverDate').dirty || this.issueDateForm.get('CoverDate').touched);
  }


  ngAfterViewInit(): void {
    this.issueDatesdtTrigger.next();
  }


  ngOnDestroy() {
    this.issueDatesdtTrigger.unsubscribe();
  }

  issueDateFormValidation() {
    this.issueDateForm = this.formBuilder.group({
      CoverDate: [null, Validators.required],
      AdClosingDate: [null],
      MaterialDueDate: [null],
      IssueName: [null],
      IssueCode: [null]
    });
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
    
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      //this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getMediaAssetsId() {
    try {
      this.mediaAssetsId = this.globalClass.getMediaAssetID()[0];
      this.mediaAssetsName = this.globalClass.getMediaAssetID()[1];
      if (this.mediaAssetsId === undefined || this.mediaAssetsId == null || this.mediaAssetsId === '') {
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  searchIssueDates()
  {
      let searchText='';
      searchText=(document.getElementById("txtSearchIssueDate") as HTMLInputElement).value;
      this.getIssueDate(searchText);
  }

  // GetIssue Date Data
  getIssueDate(searchText) {
    try {
      this.showLoader();
      this.issueDatesModel.IssueDateData=[];
      this.issueDatesService.getIssueDatesBySearchText(this.mediaAssetsId,searchText).subscribe(
        data => {
          this.hideLoader();
          this.issueDatesModel.IssueDateData = data.Data[0].IssueDates;
          this.issueDatesdtTrigger.next();
        },
        error => {
          this.hideLoader();
          console.log(error);
        });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  onIssueDateChange(value: Date): void {
    try {
      var issueDate = value.toString();
      if(issueDate==='Invalid Date')
      {
        this.toastr.error('Please select valid issue date', 'Error');
      }
      else
      {
        const tempDate = new Date();
        tempDate.setDate(value.getDate());
        this.maxAdClosingDate = tempDate;
        this.maxMaterialsDueDate = tempDate;
      }
    } catch (error) {
      this.hideLoader();
      console.log("onIssueDateChange:"+error);
    }
  }

  resetissueDates() {
    this.issueDatesModel.IssueDateId = 0;
    this.issueDateForm.reset();
    document.querySelector('#btnIssueDates').innerHTML = 'Create';
    document.getElementById('btnIssueDates').style.display = 'inline-block';
  }

  editIssueDate(IssueDateId, CoverDate, AdClosingDate,
    MaterialDueDate, IssueName, IssueCode) {
    try {
      this.issueDatesModel.IssueDateId = IssueDateId;

      if (CoverDate === undefined || CoverDate == null || CoverDate === '') {
        this.issueDateForm.controls['CoverDate'].setValue(null);
      } else {
        this.issueDateForm.controls['CoverDate'].setValue(new Date(CoverDate));
      }

      if (AdClosingDate === undefined || AdClosingDate == null || AdClosingDate === '') {
        this.issueDateForm.controls['AdClosingDate'].setValue(null);
      } else {
        this.issueDateForm.controls['AdClosingDate'].setValue(new Date(AdClosingDate));
      }

      if (MaterialDueDate === undefined || MaterialDueDate == null || MaterialDueDate === '') {
        this.issueDateForm.controls['MaterialDueDate'].setValue(null);
      } else {
        this.issueDateForm.controls['MaterialDueDate'].setValue(new Date(MaterialDueDate));
      }

      this.issueDateForm.controls['IssueName'].setValue(IssueName);
      this.issueDateForm.controls['IssueCode'].setValue(IssueCode);
      document.querySelector('#btnIssueDates').innerHTML = 'Update';
      document.getElementById('btnIssueDates').style.display = 'inline-block';
    } catch (error) {
      console.log(error);
    }
  }

  /*---------------------------------------------------------*/
  saveIssueDate() {
    try {
      let coverDate_date = this.issueDateForm.value['CoverDate'];
      let txtAdClosingDate = this.issueDateForm.value['AdClosingDate'];
      let txtMaterialDueDate = this.issueDateForm.value['MaterialDueDate'];
      const issuename = this.issueDateForm.value['IssueName'];
      const issuecode = this.issueDateForm.value['IssueCode'];

      if (coverDate_date === undefined || coverDate_date == null || coverDate_date === '') {
        coverDate_date = null;
      } else {
        coverDate_date = this.globalClass.getDate(coverDate_date);
      }

      if (txtAdClosingDate === undefined || txtAdClosingDate == null || txtAdClosingDate === '') {
        txtAdClosingDate = null;
      } else {
        txtAdClosingDate = this.globalClass.getDate(txtAdClosingDate);
      }

      if (txtMaterialDueDate === undefined || txtMaterialDueDate == null || txtMaterialDueDate === '') {
        txtMaterialDueDate = null;
      } else {
        txtMaterialDueDate = this.globalClass.getDate(txtMaterialDueDate);
      }

      try {
        this.issueDatesModel.isIssueDateFormSubmitted = true;

        if (this.issueDateForm.valid) {
          if (this.issueDatesModel.IssueDateId === 0) {
            // Add new record
            this.issueDatesService.addissueDates(this.mediaAssetsId, coverDate_date, txtAdClosingDate,
              txtMaterialDueDate, issuename, issuecode).subscribe(result => {
                if (result.StatusCode === 1) {
                  try {
                    this.issueDatesModel.isIssueDateFormSubmitted = false;
                    this.toastr.success('Saved successfully.', 'Success!');
                    this.resetissueDates();
                  } catch (error) {
                    this.hideLoader();
                    console.log(error);
                  }

                  try {
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.destroy();
                      this.getIssueDate('');
                    });
                  } catch (error) {
                    this.hideLoader();
                    console.log(error);
                  }


                } else {
                  this.issueDatesModel.isIssueDateFormSubmitted = false;
                  this.toastr.error(result.Message, 'Error');
                }
              });
          } else {
            this.addOREdit = 'Edit';
            this.issueDatesService.updateIssueDates(this.issueDatesModel.IssueDateId, this.mediaAssetsId, coverDate_date, txtAdClosingDate,
              txtMaterialDueDate, issuename, issuecode).subscribe(result => {
                if (result.StatusCode === 1) {
                  try {
                    this.issueDatesModel.isIssueDateFormSubmitted = false;
                    this.toastr.success('Saved successfully.', 'Success!');
                    this.resetissueDates();
                  } catch (error) {
                    this.hideLoader();
                    console.log(error);
                  }

                  try {
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.destroy();
                      this.getIssueDate('');
                    });
                  } catch (error) {
                    this.hideLoader();
                    console.log(error);
                  }
                } else {
                  this.issueDatesModel.isIssueDateFormSubmitted = false;
                  this.toastr.error(result.Message, 'Error');
                }
              });
          }
        }
      } catch (error) {
        this.hideLoader();
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  delete(IssueDateId, idx) {
    try {
      if (confirm('Are you sure you want to delete ')) {
        this.issueDatesService.deleteMessage(IssueDateId).subscribe(data => {
          if (data.StatusCode === 1) {
            this.toastr.success('Deleted successfully', 'Success!');
            this.resetissueDates();

            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.getIssueDate('');
            });

          } else {
            this.toastr.error(data.Message, 'Error');
          }
        }, error => {
          this.hideLoader();
          if (error.status === 0) {
            this.toastr.error('Internal server error !');
          } else {
            this.toastr.error('Something went wrong.', 'Error');
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

}
