import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RateCardService {

  public headers: HttpHeaders;
  public headers_token: HttpHeaders;
  public sfheaders: HttpHeaders;
  public headersT: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.headersT = new HttpHeaders({
      'Content-Type': 'application/json',
      RequestVerificationToken: 'token'
    });
  }

  // Bind media assest list of data
  getRateCard(mediaAssetsId: number): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/RateCard/GetByMediaAsset/` + mediaAssetsId);
  }
  getRateCardBySearchText(mediaAssetsId: number,searchText): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/RateCard/GetByMediaAssetBySearchText?id=` + mediaAssetsId+ `&searchText=`+ searchText);
  }
  getAdType(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/AdType`);
  }
  getMediaType(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaType`);
  }
  getMediaBilling(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaBillingMethod`);
  }
  // Add new media asset group
  addAdType(adTypeName: string): Observable<any> {

    const body = JSON.stringify({ AdTypeId: 0, Name: adTypeName });
    return this.http.post<any>(`${environment.ApiBaseURL}/AdType`, body, { headers: this.headers });
  }
  addRateCard(RateCardName: any, AdType: any, MediaType: any, MediaBillingMethod: any,
    ValidFromDate: any, ValidToDate: any, mediaAssetsId) {
    const body = JSON.stringify({
      RateCardId: 0,
      MediaAssetId: mediaAssetsId,
      IsCopy: true,
      SourceRateCardId: 0,
      AdTypeId: AdType,
      MediaTypeId: MediaType,
      MediaBillingMethodId: MediaBillingMethod,
      ValidFromDate: ValidFromDate,
      ValidToDate: ValidToDate,
      ProductCode: 'code3',
      Name: RateCardName
      });
      return this.http.post<any>(`${environment.ApiBaseURL}/RateCard`, body, { headers: this.headers });
  }
  updateRateCard(RateCardId: any, RateCardName: any, AdType: any, MediaType: any, MediaBillingMethod: any,
    ValidFromDate: any, ValidToDate: any, mediaAssetsId) {
      const body = JSON.stringify({
        RateCardId: RateCardId,
        MediaAssetId: mediaAssetsId,
        IsCopy: true,
        SourceRateCardId: 0,
        AdTypeId: AdType,
        MediaTypeId: MediaType,
        MediaBillingMethodId: MediaBillingMethod,
        ValidFromDate: ValidFromDate,
        ValidToDate: ValidToDate,
        ProductCode: 'code3',
        Name: RateCardName
        });
        return this.http.put<any>(`${environment.ApiBaseURL}/RateCard`, body, { headers: this.headers });
  }
  deleteMessage(RateCardId: any): Observable<any> {
    return this.http.delete(`${environment.ApiBaseURL}/RateCard/` + RateCardId);
  }
}
