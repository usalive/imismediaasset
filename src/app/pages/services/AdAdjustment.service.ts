import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdAdjustmentService {

  public headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      RequestVerificationToken: 'token'
    });
  }

  // Bind media assest list of data
  getAdjustmentByMediaAsset(id): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/AdAdjustment/GetByMediaAsset/` + id);
  }
  getAdjustmentByMediaAssetNSearchText(id,searchText): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/AdAdjustment/GetByMediaAssetAndSearchText?id=` + id +`&searchText=`+ searchText);
  }
  getAdjustmentTarget(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/AdAdjustmentTarget`);
  }
  // Add new media asset group
  saveAdjustments(amountPercent: any, surchargeDiscount: any, mediaAssetsId: any, productCode: any, adjustment: any,
    adjustmentstarget: any, adjustmentsvalue: any): Observable<any> {
    const body = JSON.stringify({
      AdAdjustmentId: 0, AmountPercent: amountPercent,
      SurchargeDiscount: surchargeDiscount, MediaAssetId: mediaAssetsId,
      ProductCode: productCode, Name: adjustment,
      AdjustmentTarget: adjustmentstarget, AdjustmentValue: adjustmentsvalue
    });
    console.log("saveAdjustement:"+ JSON.stringify(body));
    return this.http.post<any>(`${environment.ApiBaseURL}/AdAdjustment`, body, { headers: this.headers });
  }
  updateAdjustments(AdAdjustmentId: any, amountPercent: any, surchargeDiscount: any, mediaAssetsId: any, productCode: any, adjustment: any,
    adjustmentstarget: any, adjustmentsvalue: any): Observable<any> {
    const body = JSON.stringify({
      AdAdjustmentId: AdAdjustmentId, AmountPercent: amountPercent,
      SurchargeDiscount: surchargeDiscount, MediaAssetId: mediaAssetsId,
      ProductCode: productCode, Name: adjustment,
      AdjustmentTarget: adjustmentstarget, AdjustmentValue: adjustmentsvalue
    });
    return this.http.put<any>(`${environment.ApiBaseURL}/AdAdjustment`, body, { headers: this.headers });
  }
  deleteMessage(AdAdjustmentId: any): Observable<any> {
    return this.http.delete(`${environment.ApiBaseURL}/AdAdjustment/` + AdAdjustmentId);
  }
}
