import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaAssetsService {

  public headers: HttpHeaders;
  public headers_token: HttpHeaders;


  private subjectForActiveTab = new Subject<any>();


  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      RequestVerificationToken: 'token'
    });
  }


  /* Function for get active tab */
  set_ActiveTab(message: string) {
    this.subjectForActiveTab.next({ text: message });
  }
  get_ActiveTab(): Observable<any> {
    return this.subjectForActiveTab.asObservable();
  }

  getMediaAssets(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaAsset`);
  }

  getMediaAssetsViaSearch(searchText): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaAsset/GetMediaAssetViaSearch?searchText=`+ searchText);
  }

  // Bind popup dropdown
  getAllItemSummary(websiteRoot, authToken, Offset): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': authToken
    });
    return this.http.get(websiteRoot + `api/Itemsummary?limit=500&Offset=` + Offset, { headers: this.headers_token });
  }


  // Bind popup dropdown
  getMediaAssetGroupData(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaAssetGroup`);
  }


  // Bind popup dropdown
  getMediaTypeData(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/MediaType`);
  }

//Add new media type
addType(MediaAssetTypeName: string): Observable<any> {
  const body = JSON.stringify({ Name: MediaAssetTypeName });
  return this.http.post<any>(`${environment.ApiBaseURL}/MediaType`, body, { headers: this.headers });
}
  // Add new media asset group
  addGroup(MediaAssetGroupName: string): Observable<any> {
    const body = JSON.stringify({ Name: MediaAssetGroupName });
    return this.http.post<any>(`${environment.ApiBaseURL}/MediaAssetGroup`, body, { headers: this.headers });
  }


  // add new media asset
  addMediaAssets(MediaAssetsForm: any, logo: any): Observable<any> {
    const body = JSON.stringify({
      Logo: logo,
      MediaAssetGroupId: MediaAssetsForm.MediaGroup,
      MediaAssetId: 0,
      Name: MediaAssetsForm.MediaAssetName,
      MediaCode: MediaAssetsForm.MediaCode,
      MediaTypeId: MediaAssetsForm.MediaTypeName,
      ProductCode: MediaAssetsForm.ProductCode
    });
    return this.http.post<any>(`${environment.ApiBaseURL}/MediaAsset`, body, { headers: this.headers });
  }

  // update new media asset
  updateMediaAssets(MediaAssetsForm: any, mediaAssetId: any, logo: any) {
    const body = JSON.stringify({
      Logo: logo,
      MediaAssetGroupId: MediaAssetsForm.MediaGroup,
      MediaAssetId: mediaAssetId,
      Name: MediaAssetsForm.MediaAssetName,
      MediaCode: MediaAssetsForm.MediaCode,
      MediaTypeId: MediaAssetsForm.MediaTypeName,
      ProductCode: MediaAssetsForm.ProductCode
    });
    return this.http.put<any>(`${environment.ApiBaseURL}/MediaAsset`, body, { headers: this.headers });
  }


  deleteMessage(mediaAssetId: any): Observable<any> {
    return this.http.delete(`${environment.ApiBaseURL}/MediaAsset/` + mediaAssetId);
  }
}
