import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RateCardDetailsService {


  public headers: HttpHeaders;
  public headers_token: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      RequestVerificationToken: 'token'
    });
  }

    // Bind GetAdColor Data
    getByRateCardId(RateCardId): Observable<any> {
      return this.http.get(`${environment.ApiBaseURL}/RateCardDetail/GetByRateCard/` + RateCardId);
    }

    getRateCardMediaBillingMethod(rateCardId):Observable<any>
    {
      return this.http.get(`${environment.ApiBaseURL}/RateCard/` + rateCardId);
    }

  // Bind GetAdColor Data
  getAdColor(): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/AdColor`);
  }

  // Bind GetAdColor Data
  getAdSizes(mediaAssetsId: string | number): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/AdSize/GetByMediaAsset/` + mediaAssetsId);
  }

    // Bind GetAdColor Data
    getFrequency(): Observable<any> {
      return this.http.get(`${environment.ApiBaseURL}/Frequency`);
    }

  // Add new AddSaveAdColor
  addSaveAdColor(AdColorName: any): Observable<any> {

    const body = JSON.stringify({ AdColorId: 0,Name: AdColorName });
    return this.http.post<any>(`${environment.ApiBaseURL}/AdColor`, body, { headers: this.headers_token });
  }
  // Add new AddSaveAdSize
  addSaveAdSize(AdSizeName: any, PageFraction: any, mediaAssetsId: number): Observable<any> {
    const body = JSON.stringify({ AdSizeId: 0, Name: AdSizeName, PageFraction: PageFraction, MediaAssetId: mediaAssetsId  });
    return this.http.post<any>(`${environment.ApiBaseURL}/AdSize`, body, { headers: this.headers_token });
  }
  // Add new AddSaveFrequency
  addSaveFrequency(FrequencyName: any): Observable<any> {

    const body = JSON.stringify({ FrequencyId: 0, Name: FrequencyName });
    return this.http.post<any>(`${environment.ApiBaseURL}/Frequency`, body, { headers: this.headers_token });
  }


  getByRateCardAndColor(rateCardId, colorid): Observable<any> {
    
    return this.http.get(`${environment.ApiBaseURL}/RateCardDetail/GetByRateCardAndColor/` + rateCardId + '/' + colorid);
  }

  save(rateCardDetails: any): Observable<any> {
   
    const body = JSON.stringify({
        RateCardId: rateCardDetails.RateCardId,
        AdTypeId: rateCardDetails.AdTypeId,
        MediaAssetId: rateCardDetails.MediaAssetId,
        RateCardName : rateCardDetails.RateCardName,
        AdTypeName : rateCardDetails.AdTypeName,
        MediaAssetName : rateCardDetails.MediaAssetName,
        RateCardDetailCost : rateCardDetails.RateCardDetailCost,
        AdColorId : rateCardDetails.AdColorId
    });
    console.log("saveRateCardDetails:"+body);
    return this.http.post<any>(`${environment.ApiBaseURL}/RateCardDetail`, body, { headers: this.headers_token });
  }

  getByMediaAsset(mediaAssetsId: any): Observable<any> {
    return this.http.get<any>(`${environment.ApiBaseURL}/IssueDate/GetByMediaAssetForDropdown/` + mediaAssetsId);
  }

  getByMediaAssetId(mediaAssetsId: any): Observable<any> {
    return this.http.get<any>(`${environment.ApiBaseURL}/Inventory/GetByMediaAsset/` + mediaAssetsId);
  }

}
