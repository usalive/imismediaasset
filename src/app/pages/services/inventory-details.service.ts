import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InventoryDetailsService {

  baseAPI = environment.ApiBaseURL;
  inventoryApi = this.baseAPI + '/Inventory';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  public headersJ: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });

    this.headersJ = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
    });
  }

  getByMediaAssetId(mediaAssetId): Observable<any> {
    return this.http.get(this.inventoryApi + '/GetByMediaAsset/' + mediaAssetId, { headers: this.sfheaders });
  }

  save(inventoryDetails: any): Observable<any> {
    const body = JSON.stringify({ MediaAssetId: inventoryDetails.mediaAssetsId, Inventory: inventoryDetails.Inventory });
  
    return this.http.post(this.inventoryApi, body, { headers: this.headersJ });
  }
}
