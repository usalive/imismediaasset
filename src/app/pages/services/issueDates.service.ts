import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IssueDatesService {

  public headers: HttpHeaders;
  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  // Bind media assest list of data
  getIssueDates(mediaAssetId: string): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/IssueDate/GetByMediaAssetForDropdown/` + mediaAssetId);
  }

  getIssueDatesBySearchText(mediaAssetId: string,searchText): Observable<any> {
    return this.http.get(`${environment.ApiBaseURL}/IssueDate/GetByMediaAssetAndSearchText?mediaAssetId=` + mediaAssetId +`&searchText=`+ searchText);
  }

  // Add new media asset group
  addissueDates(MediaAssetId: any, coverDate: any, txtAdClosingDate: any, txtMaterialDueDate: any,
    issuename: any, issuecode: any): Observable<any> {
    const body = JSON.stringify({
      IssueDateId: 0,
      MediaAssetId: MediaAssetId,
      CoverDate: coverDate,
      AdClosingDate: txtAdClosingDate,
      MaterialDueDate: txtMaterialDueDate,
      IssueName: issuename,
      IssueCode: issuecode
    });
    return this.http.post<any>(`${environment.ApiBaseURL}/IssueDate`, body, { headers: this.headers });
  }

  // update new media asset
  updateIssueDates(IssueDateId: any, MediaAssetId: any, coverDate: any, txtAdClosingDate: any, txtMaterialDueDate: any,
    issuename: any, issuecode: any): Observable<any> {
    const body = JSON.stringify({
      IssueDateId: IssueDateId, MediaAssetId: MediaAssetId, CoverDate: coverDate,
      AdClosingDate: txtAdClosingDate, MaterialDueDate: txtMaterialDueDate, IssueName: issuename, IssueCode: issuecode
    });
    return this.http.put<any>(`${environment.ApiBaseURL}/IssueDate`, body, { headers: this.headers });
  }

  deleteMessage(IssueDateId: any): Observable<any> {
    return this.http.delete(`${environment.ApiBaseURL}/IssueDate/` + IssueDateId);
  }

}
