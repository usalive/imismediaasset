// import { NgModule, Component } from '@angular/core';
// import { CommonModule } from '@angular/common';
// // import { BrowserModule } from '@angular/platform-browser';
// import { RouterModule } from '@angular/router';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { BsDatepickerModule } from 'ngx-bootstrap';
// import { NgSelectModule } from '@ng-select/ng-select';
// // Components
// import { AdAdjustmentComponent } from './components/AdAdjustment/AdAdjustment.component';
// import { InventoryDetailsComponent } from './components/inventoryDetails/inventoryDetails.component';
// import { IssueDatesComponent } from './components/issueDates/issueDates.component';
// import { MediaAssetsComponent } from './components/mediaAssets/mediaAssets.component';
// import { RateCardComponent } from './components/rateCard/rateCard.component';
// import { RateCardsDetailsComponent } from './components/rateCardsDetails/rateCardsDetails.component';
// import { FrequencyNamePipe } from './components/rateCardsDetails/FrequencyName.pipe';
// import { AdSizeNamePipe } from './components/rateCardsDetails/AdSizeName.pipe';
// import { RateCardCostPipe } from './components/rateCardsDetails/RateCardCost.pipe';
// import { RateCardsDetailsNewComponent } from './components/rateCardsDetailsNew/rate-cards-details-new.component';
// import { RateCardDetailsFinalComponent} from './components/rateCardDetailsFinal/rateCardDetailsFinal.component';
// import { SafePipe } from './components/rateCardDetailsFinal/safe.pipe';
// // services

// // Routes
// import { PagesRoutes as routes } from './pages.routes';
// import { MediaAssetsService } from './services/mediaAssets.service';
// import { HttpClientModule } from '@angular/common/http';
// import { DataTablesModule } from '../../../node_modules/angular-datatables';

// @NgModule({
//   declarations: [AdAdjustmentComponent, InventoryDetailsComponent, IssueDatesComponent,
//     RateCardsDetailsNewComponent, RateCardDetailsFinalComponent,
//     MediaAssetsComponent, RateCardComponent, RateCardsDetailsComponent, FrequencyNamePipe, AdSizeNamePipe, RateCardCostPipe, SafePipe
//   ],
//   imports: [
//     RouterModule.forRoot(routes, { useHash: true }),
//     CommonModule,
//     FormsModule,
//     ReactiveFormsModule, HttpClientModule,
//     BsDatepickerModule.forRoot(),
//     DataTablesModule, NgSelectModule
//   ],
//   providers: [MediaAssetsService]
// })
// export class PagesModule { }
