export class RateCardDetailsModel {
    isAdColorModalFormSubmitted = false;
    isAdSizeModalFormSubmitted = false;
    isFrequencyModalFormSubmitted = false;
    israteCardDetailsFormSubmitted = false;

    AdColorData = [];
    AdSizeData = [];
    FrequencyData = [];
    RateCardData = [];

    AdColorName: any;
    afterTableData = [];
}
