import { Component, OnInit, HostListener } from '@angular/core';
import { environment } from '../environments/environment';
import { Subscription } from 'rxjs';
import { MediaAssetsService } from '../app/pages/services/mediaAssets.service';

@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  screenWidth: any;
  screenHeight: any;

  mediaAssetTab1: any;
  multipleRateCardsDiv: any;

  mediaAssetPanelDiv: any;
  issueDatePanelDiv: any;
  adAdjustmentsPanelDiv: any;
  rateCardsPanelDiv: any;
  rateCardsDetailsPanelDiv: any;
  inventoryDetailsPanelDiv: any;

  mediaAssetTabContent: any;
  issueDateTabContent: any;
  adAdjustmentsTabContent: any;
  rateCardsTabContent: any;
  rateCardsDetailsTabContent: any;
  inventoryDetailsTabContent: any;

  selectedTab = 'MediaAsset';
  mediaAssetTab = true;
  issueDateTab = false;
  adAdjustmentTab = false;
  rateCardTab = false;
  rateCardsDetailsTab = false;
  inventoryDetailsTab = false;
  isLoading = true;

  mediaAssetDisable: any;
  issueDateTabDisable: any;
  adAdjustmenTabDisable: any;
  rateCardTabDisable: any;
  rateCardsDetailsTabDisable: any;
  inventoryDetailsTabDisable: any;

  setCursorPointer = true;
  subscriptionForActiveTab: Subscription;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  currentUserName = '';

  constructor(private mediaAssetsService: MediaAssetsService) {
    this.onResize();
  }

  ngOnInit() {
    try {
      let url = window.location.href;
      if (url.indexOf('#') > -1) {
        url = url.substring(0, url.lastIndexOf('#'));
      }
      location.replace(url + '#/mediaAssetsDetails');
    } catch (error) {
      console.log(error);
    }
    try {
      this.currentUserName = environment.CurrentUserName;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
    this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
     // this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }

    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.adAdjustmenTabDisable.mediaAssetTab1 = document.querySelector('.step-wizard');
        setTimeout(() => {
          this.setMediaAssetsTabActive();
        }, 30);
      }, 2000);
    } else {
      setTimeout(() => {
        this.mediaAssetTab1 = document.querySelector('.step-wizard');
      }, 2000);
      this.setMediaAssetsTabActive();
    }

    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTab1 = document.querySelector('.step-wizard');
        setTimeout(() => {
          if (this.selectedTab === 'MediaAsset') {
            this.setMediaAssetsTabActive();
          } else if (this.selectedTab === 'IssueDate') {
            this.setIssueDateTabActive();
          } else if (this.selectedTab === 'AdAdjustment') {
            this.setAdAdjustmentTabActive();
          } else if (this.selectedTab === 'RateCard') {
            this.setRateCardTabActive();
          } else if (this.selectedTab === 'RateCardsDetails') {
            this.setRateCardsDetailsTabActive();
          } else if (this.selectedTab === 'InventoryDetails') {
            this.setInventoryDetailsTabActive();
          }
        }, 30);
      }, 2000);
    } else {
      setTimeout(() => {
        this.mediaAssetTab1 = document.querySelector('.step-wizard');
       
      }, 2000);

      if (this.selectedTab === 'MediaAsset') {
        this.setMediaAssetsTabActive();
      } else if (this.selectedTab === 'IssueDate') {
        this.setIssueDateTabActive();
      } else if (this.selectedTab === 'AdAdjustment') {
        this.setAdAdjustmentTabActive();
      } else if (this.selectedTab === 'RateCard') {
        this.setRateCardTabActive();
      } else if (this.selectedTab === 'RateCardsDetails') {
        this.setRateCardsDetailsTabActive();
      } else if (this.selectedTab === 'InventoryDetails') {
        this.setInventoryDetailsTabActive();
      }
    }

    // Used for set active tab
    this.subscriptionForActiveTab = this.mediaAssetsService.get_ActiveTab().subscribe(message => {
      if (message.text === 'IssueDate') {
        this.setIssueDateTabActive();
      } else if (message.text === 'Adjustment') {
        this.setAdAdjustmentTabActive();
      } else if (message.text === 'rateCard') {
        this.setRateCardTabActive();
      } else if (message.text === 'rateCardDetails') {
        this.setRateCardsDetailsTabActive();
      } else if (message.text === 'inventoryDetails') {
        this.setInventoryDetailsTabActive();
      }
    });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
  }
  

  setMediaAssetsTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.issueDateTabContent = document.querySelector('#issue-dates-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#media-assets-tab');
        this.mediaAssetTabContent.classList.remove('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#rate-cards-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-tab');
        this.rateCardsDetailsTabContent.classList.add('collapsed');

        this.rateCardsDetailsTabContent = document.querySelector('#inventory-details-tab');
        this.rateCardsDetailsTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#issue-dates-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-collapse');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#inventory-details-collapse');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.mediaAssetTabContent = document.querySelector('#media-assets-collapse');
        this.mediaAssetTabContent.classList.add('in');
        this.mediaAssetTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.add('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.add('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

        this.rateCardsDetailsPanelDiv = this.rateCardsDetailsTabContent.closest('div.panel.panel-default');
        this.rateCardsDetailsPanelDiv.classList.add('noclick');

        this.inventoryDetailsPanelDiv = this.inventoryDetailsTabContent.closest('div.panel.panel-default');
        this.inventoryDetailsPanelDiv.classList.add('noclick');
      }, 10);
    } else {

      try {

        this.mediaAssetTabContent = document.querySelector('#media-assets');
        this.mediaAssetTabContent.classList.add('active');
        this.mediaAssetTabContent.classList.add('in');

        this.issueDateTabContent = document.querySelector('#issue-dates');
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments');
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards');
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details');
        this.rateCardsDetailsTabContent.classList.remove('active');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details');
        this.inventoryDetailsTabContent.classList.remove('active');
        this.inventoryDetailsTabContent.classList.remove('in');

      } catch (error) {

      }

      this.mediaAssetTab = true;
      this.issueDateTab = false;
      this.adAdjustmentTab = false;
      this.rateCardTab = false;
      this.rateCardsDetailsTab = false;
      this.inventoryDetailsTab = false;
    }

    this.mediaAssetDisable = false;
    this.issueDateTabDisable = true;
    this.adAdjustmenTabDisable = true;
    this.rateCardTabDisable = true;
    this.rateCardsDetailsTabDisable = true;
    this.inventoryDetailsTabDisable = true;
    this.selectedTab = 'MediaAsset';
  }

  setIssueDateTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#media-assets-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#issue-dates-tab');
        this.issueDateTabContent.classList.remove('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#rate-cards-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-tab');
        this.rateCardsDetailsTabContent.classList.add('collapsed');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-tab');
        this.inventoryDetailsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#media-assets-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates-collapse');
        this.issueDateTabContent.classList.add('in');

        this.issueDateTabContent.css('height', 'auto');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-collapse');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-collapse');
        this.inventoryDetailsTabContent.classList.remove('in');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.add('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

        this.rateCardsDetailsPanelDiv = this.rateCardsDetailsTabContent.closest('div.panel.panel-default');
        this.rateCardsDetailsPanelDiv.classList.add('noclick');

        this.inventoryDetailsPanelDiv = this.inventoryDetailsTabContent.closest('div.panel.panel-default');
        this.inventoryDetailsPanelDiv.classList.add('noclick');
      }, 10);

    } else {

      try {
        this.mediaAssetTabContent = document.querySelector('#media-assets');
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates');
        this.issueDateTabContent.classList.add('active');
        this.issueDateTabContent.classList.add('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments');
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards');
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details');
        this.rateCardsDetailsTabContent.classList.remove('active');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details');
        this.inventoryDetailsTabContent.classList.remove('active');
        this.inventoryDetailsTabContent.classList.remove('in');

      } catch (error) {
      }

      this.mediaAssetTab = false;
      this.issueDateTab = true;
      this.adAdjustmentTab = false;
      this.rateCardTab = false;
      this.rateCardsDetailsTab = false;
      this.inventoryDetailsTab = false;
    }

    this.mediaAssetDisable = false;
    this.issueDateTabDisable = false;
    this.adAdjustmenTabDisable = true;
    this.rateCardTabDisable = true;
    this.rateCardsDetailsTabDisable = true;
    this.inventoryDetailsTabDisable = false;
    this.selectedTab = 'IssueDate';
  }

  setAdAdjustmentTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#media-assets-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#issue-dates-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-tab');
        this.adAdjustmentsTabContent.classList.remove('collapsed');

        this.rateCardsTabContent = document.querySelector('#rate-cards-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-tab');
        this.rateCardsDetailsTabContent.classList.add('collapsed');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-tab');
        this.inventoryDetailsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#media-assets-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-collapse');
        this.adAdjustmentsTabContent.classList.add('in');

        this.adAdjustmentsTabContent.css('height', 'auto');

        this.rateCardsTabContent = document.querySelector('#rate-cards-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-collapse');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-collapse');
        this.inventoryDetailsTabContent.classList.remove('in');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

        this.rateCardsDetailsPanelDiv = this.rateCardsDetailsTabContent.closest('div.panel.panel-default');
        this.rateCardsDetailsPanelDiv.classList.add('noclick');

        this.inventoryDetailsPanelDiv = this.inventoryDetailsTabContent.closest('div.panel.panel-default');
        this.inventoryDetailsPanelDiv.classList.add('noclick');
      }, 10);
    } else {

      try {

        this.mediaAssetTabContent = document.querySelector('#media-assets');
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates');
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments');
        this.adAdjustmentsTabContent.classList.add('active');
        this.adAdjustmentsTabContent.classList.add('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards');
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details');
        this.rateCardsDetailsTabContent.classList.remove('active');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details');
        this.inventoryDetailsTabContent.classList.remove('active');
        this.inventoryDetailsTabContent.classList.remove('in');

      } catch (error) {
      }

      this.mediaAssetTab = false;
      this.issueDateTab = false;
      this.adAdjustmentTab = true;
      this.rateCardTab = false;
      this.rateCardsDetailsTab = false;
      this.inventoryDetailsTab = false;
    }

    this.mediaAssetDisable = false;
    this.issueDateTabDisable = false;
    this.adAdjustmenTabDisable = false;
    this.rateCardTabDisable = true;
    this.rateCardsDetailsTabDisable = true;
    this.inventoryDetailsTabDisable = false;
    this.selectedTab = 'AdAdjustment';
  }

  setRateCardTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#media-assets-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#issue-dates-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#rate-cards-tab');
        this.rateCardsTabContent.classList.remove('collapsed');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-tab');
        this.rateCardsDetailsTabContent.classList.add('collapsed');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-tab');
        this.inventoryDetailsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#media-assets-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards-collapse');
        this.rateCardsTabContent.classList.add('in');

        this.rateCardsTabContent.css('height', 'auto');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-collapse');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-collapse');
        this.inventoryDetailsTabContent.classList.remove('in');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.remove('noclick');

        this.rateCardsDetailsPanelDiv = this.rateCardsDetailsTabContent.closest('div.panel.panel-default');
        this.rateCardsDetailsPanelDiv.classList.add('noclick');

        this.inventoryDetailsPanelDiv = this.inventoryDetailsTabContent.closest('div.panel.panel-default');
        this.inventoryDetailsPanelDiv.classList.add('noclick');

      }, 10);
    } else {

      try {
        this.mediaAssetTabContent = document.querySelector('#media-assets');
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates');
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments');
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards');
        this.rateCardsTabContent.classList.add('active');
        this.rateCardsTabContent.classList.add('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details');
        this.rateCardsDetailsTabContent.classList.remove('active');
        this.rateCardsDetailsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details');
        this.inventoryDetailsTabContent.classList.remove('active');
        this.inventoryDetailsTabContent.classList.remove('in');
      } catch (error) {
      }

      this.mediaAssetTab = false;
      this.issueDateTab = false;
      this.adAdjustmentTab = false;
      this.rateCardTab = true;
      this.rateCardsDetailsTab = false;
      this.inventoryDetailsTab = false;
    }

    this.mediaAssetDisable = false;
    this.issueDateTabDisable = false;
    this.adAdjustmenTabDisable = false;
    this.rateCardTabDisable = false;
    this.rateCardsDetailsTabDisable = true;
    this.inventoryDetailsTabDisable = false;
    this.selectedTab = 'RateCard';

  }

  setRateCardsDetailsTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.multipleRateCardsDiv = document.querySelector('#rateCardsDetailsTableforMultiple');
        this.multipleRateCardsDiv.empty();

        this.mediaAssetTabContent = document.querySelector('#media-assets-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#issue-dates-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#rate-cards-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-tab');
        this.rateCardsDetailsTabContent.classList.remove('collapsed');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-Details-tab');
        this.inventoryDetailsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#media-assets-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-Details-collapse');
        this.inventoryDetailsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-collapse');
        this.rateCardsDetailsTabContent.classList.add('in');

        this.rateCardsDetailsTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.remove('noclick');

        this.rateCardsDetailsPanelDiv = this.rateCardsDetailsTabContent.closest('div.panel.panel-default');
        this.rateCardsDetailsPanelDiv.classList.remove('noclick');

        this.inventoryDetailsPanelDiv = this.inventoryDetailsTabContent.closest('div.panel.panel-default');
        this.inventoryDetailsPanelDiv.classList.add('noclick');

      }, 10);
    } else {
      try {
        this.multipleRateCardsDiv = document.querySelector('#rateCardsDetailsTableforMultiple');
        this.multipleRateCardsDiv.empty();

        this.mediaAssetTabContent = document.querySelector('#media-assets');
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#issue-dates');
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments');
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#rate-cards');
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');

        this.inventoryDetailsTabContent = document.querySelector('#inventory-details');
        this.inventoryDetailsTabContent.classList.remove('active');
        this.inventoryDetailsTabContent.classList.remove('in');

        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details');
        this.rateCardsDetailsTabContent.classList.add('active');
        this.rateCardsDetailsTabContent.classList.add('in');

      } catch (error) {
      }

      this.mediaAssetTab = false;
      this.issueDateTab = false;
      this.adAdjustmentTab = false;
      this.rateCardTab = false;
      this.rateCardsDetailsTab = true;
      this.inventoryDetailsTab = false;
    }

    this.mediaAssetDisable = false;
    this.issueDateTabDisable = false;
    this.adAdjustmenTabDisable = false;
    this.rateCardTabDisable = false;
    this.rateCardsDetailsTabDisable = false;
    this.inventoryDetailsTabDisable = false;
    this.selectedTab = 'RateCardsDetails';
  }

  setInventoryDetailsTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#media-assets-tab');
        this.mediaAssetTabContent.classList.add('collapsed');
        this.issueDateTabContent = document.querySelector('#issue-dates-tab');
        this.issueDateTabContent.classList.add('collapsed');
        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');
        this.rateCardsTabContent = document.querySelector('#rate-cards-tab');
        this.rateCardsTabContent.classList.add('collapsed');
        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-tab');
        this.rateCardsDetailsTabContent.classList.add('collapsed');
        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-tab');
        this.inventoryDetailsTabContent.classList.remove('collapsed');

        this.mediaAssetTabContent = document.querySelector('#media-assets-collapse');
        this.mediaAssetTabContent.classList.remove('in');
        this.issueDateTabContent = document.querySelector('#issue-dates-collapse');
        this.issueDateTabContent.classList.remove('in');
        this.adAdjustmentsTabContent = document.querySelector('#ad-adjustments-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');
        this.rateCardsTabContent = document.querySelector('#rate-cards-collapse');
        this.rateCardsTabContent.classList.remove('in');
        this.rateCardsDetailsTabContent = document.querySelector('#rate-cards-details-collapse');
        this.rateCardsDetailsTabContent.classList.remove('in');
        this.inventoryDetailsTabContent = document.querySelector('#inventory-details-collapse');
        this.inventoryDetailsTabContent.classList.add('in');
        this.inventoryDetailsTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');
        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');
        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');
        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.remove('noclick');
        this.rateCardsDetailsPanelDiv = this.rateCardsDetailsTabContent.closest('div.panel.panel-default');
        this.rateCardsDetailsPanelDiv.classList.remove('noclick');
        this.inventoryDetailsPanelDiv = this.inventoryDetailsTabContent.closest('div.panel.panel-default');
        this.inventoryDetailsPanelDiv.classList.remove('noclick');
      }, 10);
    } else {
      this.mediaAssetTab = false;
      this.issueDateTab = false;
      this.adAdjustmentTab = false;
      this.rateCardTab = false;
      this.rateCardsDetailsTab = false;
      this.inventoryDetailsTab = true;
    }

    this.mediaAssetDisable = false;
    this.issueDateTabDisable = false;
    this.adAdjustmenTabDisable = false;
    this.rateCardTabDisable = false;
    this.rateCardsDetailsTabDisable = true;
    this.inventoryDetailsTabDisable = false;
    this.selectedTab = 'InventoryDetails';
  }

}
