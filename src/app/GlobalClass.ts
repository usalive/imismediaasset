import { Injectable } from '@angular/core';
@Injectable()
export class DataTablesResponse {
    data: any[];
    draw: number;
    recordsFiltered: number;
    recordsTotal: number;
}
export class GlobalClass {
    RateCardId: any;
    RateCardName: string;
    AdTypeName: string;

    MediaID: string;
    MediaName: string;

    MediaAssetID: string;
    MediaAssetName: string;
    constructor() { }
    getMediaAssetID(): any {
        return [this.MediaAssetID, this.MediaAssetName];
    }
    setMediaAssetID(MediaAssetID, MediaAssetName) {
        this.MediaAssetID = MediaAssetID;
        this.MediaAssetName = MediaAssetName;
    }
    removeMediaAssetID() {
        this.MediaAssetID = null;
        this.MediaAssetName = null;
    }

    // Rate Card Details from Rate card to RateCardDetails Page
    getRateCardDetails(): any {
        return [ this.RateCardId, this.RateCardName, this.AdTypeName, this.MediaID, this.MediaName];
    }
    setRateCardDetails(RateCardId, RateCardName, AdTypeName,  MediaID, MediaName) {
        this.RateCardId = RateCardId;
        this.RateCardName = RateCardName;
        this.AdTypeName = AdTypeName;
        this.MediaID = MediaID;
        this.MediaName = MediaName;
    }
    removeRateCardDetails() {
        this.RateCardName = null;
        this.AdTypeName = null;
    }

    getDate(dateTime) {
        const coverDate_yy = new Date(dateTime).getFullYear();
        const coverDate_mm = new Date(dateTime).getMonth() + 1;
        const coverDate_dd = new Date(dateTime).getDate();
        const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
        return coverDate;
    }

    getTwoValDate(dateTime) {
        const coverDate_yy = new Date(dateTime).getFullYear();
        let coverDate_mm = (new Date(dateTime).getMonth() + 1).toString();
        let coverDate_dd = (new Date(dateTime).getDate()).toString();

        const tempMM = coverDate_mm.toString();
        if (tempMM !== null || tempMM !== undefined || tempMM !== 'undefined') {
            if (tempMM.length === 1) {
                coverDate_mm = '0' + coverDate_mm;
            }
        }

        const tempDD = coverDate_dd.toString();
        if (tempDD !== null || tempDD !== undefined || tempDD !== 'undefined') {
            if (tempDD.length === 1) {
                coverDate_dd = '0' + coverDate_dd;
            }
        }

        const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
        return coverDate;
    }
}
